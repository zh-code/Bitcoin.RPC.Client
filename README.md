# Bitcoin.RPC.Client

Bitcoin RPC Client

# Information
[Blog page](https://zh-code.com/2019/04/24/bitcoin-rpc-lib-for-net/)

# Usage
```
BitcoinRPCClient client = new BitcoinRPCClient("ip:port", "rpcuser", "rpcpass");
```

## Example
```
BitcoinRPCClient client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
```

# Donation
Support from anyone is much appreciated!
Bitcoin: `1DX5dqq5N8mFqPRrLeANhqDs5ShwP15xTv`