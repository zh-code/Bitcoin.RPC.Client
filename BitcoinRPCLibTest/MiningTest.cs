﻿using BitcoinRPCLib;
using NUnit.Framework;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class MiningTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        public async Task GetBlockTemplate()
        {
            var obj = await client.Mining.GetBlockTemplate();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetMiningInfo()
        {
            var obj = await client.Mining.GetMiningInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(120, -1)]
        [TestCase(-1, -1)]
        public async Task GetNetworkHashPS(int blocks, int height)
        {
            var obj = await client.Mining.GetNetworkHashPS(blocks, height);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("fe0165147da737e16f5096ab6c1709825217377a95a882023ed089a89af4cff9", 0, 0)]
        public async Task PrioritiseTransaction(string txid, int priority, long fee)
        {
            var obj = await client.Mining.PrioritiseTransaction(txid, priority, fee);

            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("02000000df11c014a8d798395b5059c722ebdf3171a4217ead71bf6e0e99f4c7000000004a6f6a2db225c81e77773f6f0457bcb05865a94900ed11356d0b75228efb38c7785d6053ffff001d005d43700101000000010000000000000000000000000000000000000000000000000000000000000000ffffffff0d03b477030164062f503253482fffffffff0100f9029500000000232103adb7d8ef6b63de74313e0cd4e07670d09a169b13e4eda2d650f529332c47646dac00000000")]
        public async Task SubmitBlock(string block, object parameters = null)
        {
            var obj = await client.Mining.SubmitBlock(block, parameters);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }
    }
}