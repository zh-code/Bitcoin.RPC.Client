﻿using BitcoinRPCLib;
using BitcoinRPCLib.Model.Wallet;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class WalletTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        [TestCase("fa3970c341c9f5de6ab13f128cbfec58d732e736a505fe32137ad551c799ecc4")]
        public async Task AbandonTransaction(string txid)
        {
            var obj = await client.Wallet.AbandonTransaction(txid);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Invalid or non-wallet transaction id", obj.Error.Message);
        }

        [Test]
        [TestCase("1BRo7qrYHMPrzdBDzfjmzteBdYAyTMXW75")]
        public async Task AddWitnessAddress(string address)
        {
            var obj = await client.Wallet.AddWitnessAddress(address);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("addwitnessaddress is deprecated and will be fully removed in v0.17. To use addwitnessaddress in v0.16, restart bitcoind with -deprecatedrpc=addwitnessaddress.\nProjects should transition to using the address_type argument of getnewaddress, or option -addresstype=[bech32|p2sh-segwit] instead.\n",
                obj.Error.Message);
        }

        [Test]
        [TestCase(2, "mjbLRSidW1MY8oubvs4SMEnHNFXxCcoehQ", "02ecd2d250a76d204011de6bc365a56033b9b3a149f679bc17205555d3c2b2854f", "mt17cV37fBqZsnMmrHnGCm9pM28R1kQdMG")]
        public async Task AddMultiSigAddress(int numSigRequired, params string[] keysOrAddresses)
        {
            var obj = await client.Wallet.AddMultiSigAddress(numSigRequired, keysOrAddresses.ToList());

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Result);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("no full public key for address mjbLRSidW1MY8oubvs4SMEnHNFXxCcoehQ", obj.Error.Message);
        }

        [Test]
        [TestCase("/tmp/backup.dat")]
        public async Task BackupWallet(string destination)
        {
            var obj = await client.Wallet.BackupWallert(destination);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task BumpFee()
        {
            var obj = await client.Wallet.BumpFee("d4a33e0cabaz723149e1fcab4e033a4017388a644c65370e3cb06ba2f0e13975",
                new BumpFeeOption()
                {
                    TotalFee = 4000,
                    Replaceable = false
                });

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("moQR7i8XM4rSGoNwEsw3h4YEuduuP6mxw7")]
        public async Task DumpPrivKey(string address)
        {
            var obj = await client.Wallet.DumpPrivKey(address);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Private key for address moQR7i8XM4rSGoNwEsw3h4YEuduuP6mxw7 is not known", obj.Error.Message);
        }

        [Test]
        [TestCase("/tmp/dump.txt")]
        public async Task DumpWallet(string fileName)
        {
            var obj = await client.Wallet.DumpWallet(fileName);

            Assert.IsNotNull(obj);
            if (obj.Error != null)
            {
                Assert.AreEqual("/tmp/dump.txt already exists. If you are sure this is what you want, move it out of the way first", obj.Error.Message);
            }
        }

        //[Test]
        //[TestCase("test")]
        //public async Task EncryptWallet(string passPhrase)
        //{
        //    var obj = await client.Wallet.EncryptWallet(passPhrase);

        //    Assert.IsNotNull(obj);
        //    Assert.IsNull(obj.Error);
        //}

        [Test]
        [TestCase("")]
        public async Task GetAccountAddress(string account)
        {
            var obj = await client.Wallet.GetAccountAddress(account);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("getaccountaddress is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase("")]
        public async Task GetAccount(string address)
        {
            var obj = await client.Wallet.GetAccount(address);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("getaccount is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase("doc test")]
        public async Task GetAddressesByAccount(string account)
        {
            var obj = await client.Wallet.GetAccount(account);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("*")]
        public async Task GetBalance(string account)
        {
            var obj = await client.Wallet.GetAccount(account);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("getaccount is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase("doc test")]
        public async Task GetNewAddress(string account = "", AddressType type = AddressType.Default)
        {
            var obj = await client.Wallet.GetNewAddress(account, type);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetRawChangeAddress()
        {
            var obj = await client.Wallet.GetRawChangeAddress();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("doc test", 6)]
        public async Task GetReceivedByAccount(string account, int minConfs)
        {
            var obj = await client.Wallet.GetReceivedByAccount(account, minConfs);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("getreceivedbyaccount is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase("mjSk1Ny9spzU2fouzYgLqGUD8U41iR35QN", 6)]
        public async Task GetReceivedByAddress(string address, int minConfs)
        {
            var obj = await client.Wallet.GetReceivedByAddress(address, minConfs);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Address not found in wallet", obj.Error.Message);
        }

        [Test]
        [TestCase("5a7d24cd665108c66b2d56146f244932edae4e2376b561b3d396d5ae017b9589", false)]
        public async Task GetTransaction(string txid, bool includeWatchOnly = false)
        {
            var obj = await client.Wallet.GetTransaction(txid, includeWatchOnly);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Invalid or non-wallet transaction id", obj.Error.Message);
        }

        [Test]
        public async Task GetUnconfirmedBalance()
        {
            var obj = await client.Wallet.GetUnconfirmedBalance();

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
            Assert.GreaterOrEqual(obj.Result, 0);
        }

        [Test]
        public async Task GetWalletInfo()
        {
            var obj = await client.Wallet.GetWalletInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("muhtvdmsnbQEPFuEmxcChX58fGvXaaUoVt", "watch-only test", true)]
        public async Task ImportAddress(string addressOrScript, string account = "", bool rescan = true)
        {
            var obj = await client.Wallet.ImportAddress(addressOrScript, account, rescan);

            Assert.IsNotNull(obj);
        }

        [Test]
        public async Task ImportMultiAddress()
        {
            var obj = await client.Wallet.ImportMultiAddress(new List<ImportAddress>()
            {
                new ImportAddress{
                    ScriptPubKey = new ImportAddress.ScriptPubKeyModel
                    {
                        Address = "1NL9w5fP9kX2D9ToNZPxaiwFJCngNYEYJo"
                    },
                    Timestamp = 0,
                    label = "Personal"
                }
            }, false);
            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task ImportMultiScript()
        {
            var obj = await client.Wallet.ImportMultiScript(new List<ImportScript>()
            {
                new ImportScript{
                    ScriptPubKey = "76a9149e857da0a5b397559c78c98c9d3f7f655d19c68688ac",
                    Timestamp = 1493912405,
                    label = "TestFailure"
                }
            }, false);
            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("txhex", "txoutproof")]
        public async Task ImportPrunedFunds(string rawTransaction, string txOutProof)
        {
            var obj = await client.Wallet.ImportPrunedFunds(rawTransaction, txOutProof);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("cU8Q2jGeX3GNKNa5etiC8mgEgFSeVUTRQfWE2ZCzszyqYNK4Mepy", "test label", true)]
        public async Task ImportPrivKey(string privateKey, string account = "", bool rescan = true)
        {
            var obj = await client.Wallet.ImportPrivKey(privateKey, account, rescan);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("/tmp/dump.txt")]
        public async Task ImportWallet(string fileName)
        {
            var obj = await client.Wallet.ImportWallet(fileName);

            Assert.IsNotNull(obj);
        }

        [Test]
        [TestCase(100)]
        public async Task KeyPoolRefill(int keyPoolSize)
        {
            var obj = await client.Wallet.KeyPoolRefill(keyPoolSize);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(1, true)]
        public async Task ListAccounts(int minConfs = 0, bool includeWatchOnly = false)
        {
            var obj = await client.Wallet.ListAccounts(minConfs, includeWatchOnly);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("listaccounts is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        public async Task ListAddressGroupings()
        {
            var obj = await client.Wallet.ListAddressGroupings();

            Assert.IsNotNull(obj);
            Assert.IsEmpty(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task ListLockUnspent()
        {
            var obj = await client.Wallet.ListLockUnspent();

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(6, true)]
        public async Task ListReceivedByAccount(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false)
        {
            var obj = await client.Wallet.ListReceivedByAccount(confirmations, includeEmpty, includeWatchOnly);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("listreceivedbyaccount is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase(6, true)]
        public async Task ListReceivedByAddress(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false)
        {
            var obj = await client.Wallet.ListReceivedByAddress(confirmations, includeEmpty, includeWatchOnly);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
            Assert.IsNotNull(obj.Result);
        }

        [Test]
        [TestCase("00000000688633a503f69818a70eac281302e9189b1bb57a76a05c329fcda718", 6, true)]
        public async Task ListSinceBlock(string headerHash = "", int targetConfirmations = 1, bool includeWatchOnly = false)
        {
            var obj = await client.Wallet.ListSinceBlock(headerHash, targetConfirmations, includeWatchOnly);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("*", 10, true)]
        public async Task ListTransactions(string account = "*", int amountOfTx = 10, bool skip = false)
        {
            var obj = await client.Wallet.ListTransactions(account, amountOfTx, skip);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Block not found", obj.Error.Message);
        }

        [Test]
        public async Task ListUnspent()
        {
            var obj = await client.Wallet.ListUnspent(6, 9999999, new List<string>() { "mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe" });

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
            Assert.IsNotNull(obj.Result);
        }

        [Test]
        public async Task LockUnspent()
        {
            var obj = await client.Wallet.LockUnspent(false, new List<LockUnspentOutput> {
                new LockUnspentOutput {
                    TxId = "5a7d24cd665108c66b2d56146f244932edae4e2376b561b3d396d5ae017b9589",
                    Vout = 0
                },
                new LockUnspentOutput{
                    TxId = "6c5edd41a33f9839257358ba6ddece67df9db7f09c0db6bbea00d0372e8fe5cd",
                    Vout = 0
                }
            });

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            //Unknown Transaction
        }

        [Test]
        [TestCase("doc test", "test1", 0.1, "Example move")]
        public async Task Move(string from, string to, decimal amount, string comment = "")
        {
            var obj = await client.Wallet.Move(from, to, amount, comment);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("move is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase("a8d0c0184dde994a09ec054286f1ce581bebf46446a512166eae7628734ea0a5")]
        public async Task RemovePrunedFunds(string txid)
        {
            var obj = await client.Wallet.RemovePrunedFunds(txid);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Transaction does not exist in wallet.", obj.Error.Message);
        }

        [Test]
        [TestCase("test", "mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe", 0.1, 6, "Example spend", "zh-code.com")]
        public async Task SendFrom(string from, string to, decimal amount, int confirmation = 1, string comment = "", string commentTo = "")
        {
            var obj = await client.Wallet.SendFrom(from, to, amount, confirmation, comment, commentTo);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("sendfrom is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        public async Task SendMany()
        {
            var obj = await client.Wallet.SendMany("test1",
                new Dictionary<string, decimal>() {
                    { "mjSk1Ny9spzU2fouzYgLqGUD8U41iR35QN", (decimal)0.1 },
                    { "mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe", (decimal)0.2 },
                }, 6);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe", 0.1, "Example spend", "zh-code.com", false)]
        public async Task SendToAddress(string to, decimal amount, string comment = "", string commentTo = "", bool autoSubtractFee = false)
        {
            var obj = await client.Wallet.SendToAddress(to, amount, comment, commentTo, autoSubtractFee);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Insufficient funds", obj.Error.Message);
        }

        [Test]
        [TestCase("mmXgiR6KAhZCyQ8ndr2BCfEq1wNG2UnyG6", "doc test")]
        public async Task SetAccount(string address, string account)
        {
            var obj = await client.Wallet.SetAccount(address, account);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("setaccount is deprecated and will be removed in V0.18. To use this command, start bitcoind with -deprecatedrpc=accounts.", obj.Error.Message);
        }

        [Test]
        [TestCase(0.001)]
        public async Task SetTxFee(decimal fee)
        {
            var obj = await client.Wallet.SetTxFee(fee);

            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("17fshh33qUze2yifiJ2sXgijSMzJ2KNEwu", "Hello, World!")]
        public async Task SignMessage(string address, string message)
        {
            var obj = await client.Wallet.SignMessage(address, message);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Invalid address", obj.Error.Message);
        }

        [Test]
        [TestCase("5HpHagT65TZzG1PH3CSu63k8DbpvD8s5ip4nEB3kEsreKamq6aB", "Hello, World!")]
        public async Task SignMessageWithPrivKey(string privKey, string message)
        {
            var obj = await client.Wallet.SignMessageWithPrivKey(privKey, message);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Invalid private key", obj.Error.Message);
        }

        [Test]
        public async Task WalletLock()
        {
            var obj = await client.Wallet.WalletLock();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Error: running with an unencrypted wallet, but walletlock was called.", obj.Error.Message);
        }

        [Test]
        [TestCase("test", 600)]
        public async Task WalletPassphrase(string passphrase, int seconds)
        {
            var obj = await client.Wallet.WalletPassphrase(passphrase, seconds);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Error: running with an unencrypted wallet, but walletpassphrase was called.", obj.Error.Message);
        }

        [Test]
        [TestCase("test", "new")]
        public async Task WalletPassphraseChange(string current, string @new)
        {
            var obj = await client.Wallet.WalletPassphraseChange(current, @new);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Error: running with an unencrypted wallet, but walletpassphrasechange was called.", obj.Error.Message);
        }
    }
}