﻿using BitcoinRPCLib;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class UtilityTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        [TestCase(2, "mjbLRSidW1MY8oubvs4SMEnHNFXxCcoehQ", "02ecd2d250a76d204011de6bc365a56033b9b3a149f679bc17205555d3c2b2854f", "mt17cV37fBqZsnMmrHnGCm9pM28R1kQdMG")]
        public async Task CreateMultiSig(int amount, params string[] keysOrAddresses)
        {
            var obj = await client.Utility.CreateMultiSig(amount, keysOrAddresses.ToList());

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Result);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase(6)]
        public async Task EstimateFee(int blocks)
        {
            var obj = await client.Utility.EstimateFee(blocks);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("estimatefee was removed in v0.17.\nClients should use estimatesmartfee.", obj.Error.Message);
        }

        [Test]
        [TestCase(6)]
        public async Task EstimatePriority(int blocks)
        {
            var obj = await client.Utility.EstimatePriority(blocks);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        public async Task GetMemoryInfo()
        {
            var obj = await client.Utility.GetMemoryInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("17fshh33qUze2yifiJ2sXgijSMzJ2KNEwu")]
        public async Task ValidateAddress(string address)
        {
            var obj = await client.Utility.ValidateAddress(address);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsFalse(obj.Result.IsValid);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe", "IL98ziCmwYi5pL+dqKp4Ux+zCa4hP/xbjHmWh+Mk/lefV/0pWV1p/gQ94jgExSmgH2/+PDcCCrOHAady2IEySSI=", "'Hello, World!'")]
        public async Task VerifyMessage(string address, string signature, string message)
        {
            var obj = await client.Utility.VerifyMessage(address, signature, message);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }
    }
}