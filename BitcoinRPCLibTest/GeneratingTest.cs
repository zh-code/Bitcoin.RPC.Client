﻿using BitcoinRPCLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class GeneratingTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        [TestCase(2)]
        [TestCase(2, 50000)]
        public async Task Generate(int blocks, int maxTries = 0)
        {
            var obj = await client.Generating.Generate(blocks, maxTries);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(2, "mtXWDB6k5yC5v7TcwKZHB89SUp85yCKshy")]
        [TestCase(2, "mtXWDB6k5yC5v7TcwKZHB89SUp85yCKshy", 50000)]
        public async Task GenerateToAddress(int blocks, string address, int maxTries = 0)
        {
            var obj = await client.Generating.GenerateToAddress(blocks, address, maxTries);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }
    }
}
