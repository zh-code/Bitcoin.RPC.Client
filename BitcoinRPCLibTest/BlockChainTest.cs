﻿using BitcoinRPCLib;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class BlockChainTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        public async Task GetBestBlockHash()
        {
            var obj = await client.BlockChain.GetBestBlockHash();

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000000000000e617fc94bc1adf5bb9ce4e97b36d4aa2e946ae65c9ecd3fd548")]
        public async Task GetBlockSerializedHex(string hash)
        {
            var obj = await client.BlockChain.GetBlockSerializedHex(hash);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrWhiteSpace(obj.Result));
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000000000000e617fc94bc1adf5bb9ce4e97b36d4aa2e946ae65c9ecd3fd548")]
        public async Task GetBlockSerializedJson(string hash)
        {
            var obj = await client.BlockChain.GetBlockSerializedJson(hash);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.GreaterOrEqual(obj.Result.Tx.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000000000000e617fc94bc1adf5bb9ce4e97b36d4aa2e946ae65c9ecd3fd548")]
        public async Task GetBlockSerializedJsonWithTx(string hash)
        {
            var obj = await client.BlockChain.GetBlockSerializedJsonWithTx(hash);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.GreaterOrEqual(obj.Result.Tx.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetBlockChainInfo()
        {
            var obj = await client.BlockChain.GetBlockChainInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetBlockCount()
        {
            var obj = await client.BlockChain.GetBlockCount();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(240886)]
        [TestCase(240887)]
        public async Task GetBlockHash(int height)
        {
            var obj = await client.BlockChain.GetBlockHash(height);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrEmpty(obj.Result));
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000000000000e617fc94bc1adf5bb9ce4e97b36d4aa2e946ae65c9ecd3fd548")]
        public async Task GetBlockHeaderHex(string hash)
        {
            var obj = await client.BlockChain.GetBlockHeaderHex(hash);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrEmpty(obj.Result));
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000000000000e617fc94bc1adf5bb9ce4e97b36d4aa2e946ae65c9ecd3fd548")]
        public async Task GetBlockHeaderJson(string hash)
        {
            var obj = await client.BlockChain.GetBlockHeaderJson(hash);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetChainTips()
        {
            var obj = await client.BlockChain.GetChainTips();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetDifficulty()
        {
            var obj = await client.BlockChain.GetDifficulty();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("328bd2540442618645b021d23603f906704146c95170bf93adcd1ed762132991")]
        [TestCase("00e38fc1c06fd54bb0874c3f5b69108ae2ff7de1c140749a6f94cc25c4dd5b28")]
        public async Task GetMemPoolAncestorsJson(string txid)
        {
            var obj = await client.BlockChain.GetMemPoolAncestorsJson(txid);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("328bd2540442618645b021d23603f906704146c95170bf93adcd1ed762132991")]
        [TestCase("00e38fc1c06fd54bb0874c3f5b69108ae2ff7de1c140749a6f94cc25c4dd5b28")]
        public async Task GetMemPoolAncestorsRawTx(string txid)
        {
            var obj = await client.BlockChain.GetMemPoolAncestorsRawTx(txid);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("328bd2540442618645b021d23603f906704146c95170bf93adcd1ed762132991")]
        [TestCase("00e38fc1c06fd54bb0874c3f5b69108ae2ff7de1c140749a6f94cc25c4dd5b28")]
        public async Task GetMemPoolDescendantsRawTx(string txid)
        {
            var obj = await client.BlockChain.GetMemPoolDescendantsRawTx(txid);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("328bd2540442618645b021d23603f906704146c95170bf93adcd1ed762132991")]
        [TestCase("00e38fc1c06fd54bb0874c3f5b69108ae2ff7de1c140749a6f94cc25c4dd5b28")]
        public async Task GetMemPoolDescendantsJson(string txid)
        {
            var obj = await client.BlockChain.GetMemPoolDescendantsJson(txid);

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("328bd2540442618645b021d23603f906704146c95170bf93adcd1ed762132991")]
        [TestCase("00e38fc1c06fd54bb0874c3f5b69108ae2ff7de1c140749a6f94cc25c4dd5b28")]
        public async Task GetMemPoolEntry(string txid)
        {
            var obj = await client.BlockChain.GetMemPoolEntry(txid);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetMemPoolInfo()
        {
            var obj = await client.BlockChain.GetMemPoolInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetRawMemPoolTxResponse()
        {
            var obj = await client.BlockChain.GetRawMemPoolTxResponse();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetRawMemPoolJsonResponse()
        {
            var obj = await client.BlockChain.GetRawMemPoolJsonResponse();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result.Keys.Count, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("4fc23d70895ed9c1d38ee52546303705b84b0822f9fa40243246d87eb0be3860")]
        [TestCase("4fc23d70895ed9c1d38ee52546303705b84b0822f9fa40243246d87eb0be3860", 0)]
        [TestCase("4fc23d70895ed9c1d38ee52546303705b84b0822f9fa40243246d87eb0be3860", 0, true)]
        public async Task GetTxOut(string txid, int vout = 0, bool unconfirmed = false)
        {
            var obj = await client.BlockChain.GetTxOut(txid, vout, unconfirmed);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("", "4fc23d70895ed9c1d38ee52546303705b84b0822f9fa40243246d87eb0be3860")]
        public async Task GetTxOutProof(string headerHash, params string[] txids)
        {
            var obj = await client.BlockChain.GetTxOutProof(txids.ToList(), headerHash);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrEmpty(obj.Result));
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetTxOutSetInfo()
        {
            var obj = await client.BlockChain.GetTxOutSetInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("7a492ec774e3355449db560cb767cf384557af7566500c5bf118b84f2e1b040d")]
        public async Task PreciousBlock(string headerHash)
        {
            var obj = await client.BlockChain.PreciousBlock(headerHash);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(413555)]
        public async Task PruneBlockChain(int height)
        {
            var obj = await client.BlockChain.PruneBlockChain(height);

            Assert.IsNotNull(obj);
            Assert.AreEqual(obj.Result, height);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(1, 2)]
        [TestCase(2, 2)]
        [TestCase(3, 2)]
        [TestCase(4, 2)]
        public async Task VerifyChain(int checkLevel, int numOfBlocks)
        {
            var obj = await client.BlockChain.VerifyChain(checkLevel, numOfBlocks);

            Assert.IsNotNull(obj);
            Assert.IsTrue(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("0000002045e48622403c2c7b607c5b7c7c6ba19da50a080a2e549376710d0000000000008d69d0ce20f370e6185c655e46e06771115091aef85150e39a1c0cd755be1e72990dbd5ca0dc0f1a3e920b415600000008a4b80d6e031fae38b1aea1a3367d1157c70749cece452c579fa7518ce7bc846ddce12e328a722e92d14fbc320dbbd7f33180cad2cdeca6002c37652fa197727d6038beb07ed846322440faf922084bb80537304625e58ed3c1d95e89703dc24ffa9ced9dfdc007fec594bcd2b76d069f321587c4080efd42a510b249f9019c3d5151439559aca57716815dec577f16300768afdc480de97dfd361f69aa16bf4fae94fdbce2f30660e7cdb4de1d49b5772ce7c3a0425633e2594be0ee746c482b58f47b2a2dd24cb920ac135e188444ecbc874a4ddd17f9393aa322768f9812d2b0a957d4a29b9de45f6e6099a91c54c38442c680cbb473ab071d60346727e78302d703")]
        public async Task VerifyTxOutProof(string proof)
        {
            var obj = await client.BlockChain.VerifyTxOutProof(proof);

            Assert.IsNotNull(obj);
            Assert.Greater(obj.Result.Count, 0);
            Assert.IsNull(obj.Error);
        }
    }
}