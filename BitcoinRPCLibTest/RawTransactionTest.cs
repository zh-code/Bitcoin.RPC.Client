﻿using BitcoinRPCLib;
using BitcoinRPCLib.Model.RawTransaction;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class RawTransactionTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        public async Task CreateRawTransaction()
        {
            List<RawTransactionInput> inputs = new List<RawTransactionInput>();
            inputs.Add(new RawTransactionInput
            {
                TxId = "1eb590cd06127f78bf38ab4140c4cdce56ad9eb8886999eb898ddf4d3b28a91d",
                Vout = 0
            });
            Dictionary<string, decimal> outputs = new Dictionary<string, decimal>();
            outputs.Add("mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe", (decimal)0.13);

            var obj = await client.RawTransaction.CreateRawTransaction(inputs, outputs);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrEmpty(obj.Result));
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task FundRawTransaction()
        {
            FundRawTransactionOption option = new FundRawTransactionOption
            {
                ChangeAddress = "mgnucj8nYqdrPFh2JfZSB1NmUThUGnmsqe",
                changePosition = 1,
                IncludeWatching = false,
                LockUnspents = true,
                FeeRate = (decimal)0.0001
            };

            var obj = await client.RawTransaction.FundRawTransaction("01000000011da9283b4ddf8d89eb996988b89ead56cecdc44041ab38bf787f1206cd90b51e0000000000ffffffff01405dc600000000001976a9140dfc8bafc8419853b34d5e072ad37d1a5159f58488ac00000000", option);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        [TestCase("0100000001bafe2175b9d7b3041ebac529056b393cf2997f7964485aa382ffa449ffdac02a000000008a473044022013d212c22f0b46bb33106d148493b9a9723adb2c3dd3a3ebe3a9c9e3b95d8cb00220461661710202fbab550f973068af45c294667fc4dc526627a7463eb23ab39e9b01410479be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8ffffffff01b0a86a00000000001976a91401b81d5fa1e55e069e3cc2db9c19e2e80358f30688ac00000000")]
        public async Task DecodeRawTransaction(string rawTransactionHex)
        {
            var obj = await client.RawTransaction.DecodeRawTransaction(rawTransactionHex);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("522103ede722780d27b05f0b1169efc90fa15a601a32fc6c3295114500c586831b6aaf2102ecd2d250a76d204011de6bc365a56033b9b3a149f679bc17205555d3c2b2854f21022d609d2f0d359e5bc0e5d0ea20ff9f5d3396cb5b1906aa9c56a0e7b5edc0c5d553ae")]
        public async Task DecodeScript(string scriptHex)
        {
            var obj = await client.RawTransaction.DecodeScript(scriptHex);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("52309405287e737cf412fc42883d65a392ab950869fae80b2a5f1e33326aca46")]
        public async Task GetRawTransaction(string txid)
        {
            var obj = await client.RawTransaction.GetRawTransaction(txid);

            Assert.IsNotNull(obj);
            Assert.AreEqual("No such mempool transaction. Use -txindex to enable blockchain transaction queries. Use gettransaction for wallet transactions.", obj.Error.Message);
        }

        [Test]
        [TestCase("52309405287e737cf412fc42883d65a392ab950869fae80b2a5f1e33326aca46")]
        public async Task GetRawTransactionVerbose(string txid)
        {
            var obj = await client.RawTransaction.GetRawTransactionVerbose(txid);

            Assert.IsNotNull(obj);
            Assert.AreEqual("No such mempool transaction. Use -txindex to enable blockchain transaction queries. Use gettransaction for wallet transactions.", obj.Error.Message);
        }

        [Test]
        [TestCase("01000000011da9283b4ddf8d89eb996988b89ead56cecdc44041ab38bf787f1206cd90b51e000000006a47304402200ebea9f630f3ee35fa467ffc234592c79538ecd6eb1c9199eb23c4a16a0485a20220172ecaf6975902584987d295b8dddf8f46ec32ca19122510e22405ba52d1f13201210256d16d76a49e6c8e2edc1c265d600ec1a64a45153d45c29a2fd0228c24c3a524ffffffff01405dc600000000001976a9140dfc8bafc8419853b34d5e072ad37d1a5159f58488ac00000000", false)]
        public async Task SendRawTransactions(string transactionHex, bool allowHighFees = false)
        {
            var obj = await client.RawTransaction.SendRawTransaction(transactionHex, allowHighFees);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
            Assert.AreEqual("Missing inputs", obj.Error.Message);
        }

        [Test]
        [TestCase("01000000011da9283b4ddf8d89eb996988b89ead56cecdc44041ab38bf787f1206cd90b51e000000006a47304402200ebea9f630f3ee35fa467ffc234592c79538ecd6eb1c9199eb23c4a16a0485a20220172ecaf6975902584987d295b8dddf8f46ec32ca19122510e22405ba52d1f13201210256d16d76a49e6c8e2edc1c265d600ec1a64a45153d45c29a2fd0228c24c3a524ffffffff01405dc600000000001976a9140dfc8bafc8419853b34d5e072ad37d1a5159f58488ac00000000")]
        public async Task SignRawTransaction(string transactionHex)
        {
            var obj = await client.RawTransaction.SignRawTransaction(transactionHex);

            Assert.IsNotNull(obj);
            Assert.AreEqual("signrawtransaction is deprecated and will be fully removed in v0.18. To use signrawtransaction in v0.17, restart bitcoind with -deprecatedrpc=signrawtransaction.\nProjects should transition to using signrawtransactionwithkey and signrawtransactionwithwallet before upgrading to v0.18",
                obj.Error.Message);
        }
    }
}