﻿using BitcoinRPCLib;
using NUnit.Framework;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class ControlTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        [TestCase("help")]
        public async Task Help(string RPC)
        {
            var obj = await client.Control.Help(RPC);

            Assert.IsNotNull(obj);
            Assert.IsFalse(string.IsNullOrWhiteSpace(obj.Result));
            Assert.IsNull(obj.Error);
        }

        //[Test]
        //public async Task Stop()
        //{
        //    var obj = await client.Control.Stop();

        //    Assert.IsNotNull(obj);
        //    Assert.IsFalse(string.IsNullOrWhiteSpace(obj.Result));
        //    Assert.IsNull(obj.Error);
        //}
    }
}