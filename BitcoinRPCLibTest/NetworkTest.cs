﻿using BitcoinRPCLib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinRPCLibTest
{
    public class NetworkTest
    {
        private BitcoinRPCClient client;

        [SetUp]
        public void Init()
        {
            client = new BitcoinRPCClient("http://192.168.0.247:18332/", "BitcoinService", "2VzqdIj3jYo5TEKcpQgxmZy-rFW9iOJjq1tR_Cr2f6k=");
            client.IgnoreSSL();
        }

        [Test]
        public async Task ClearBanned()
        {
            var obj = await client.Network.ClearBanned();

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public async Task GetAddedNodeInfo(bool details, string node = "")
        {
            var obj = await client.Network.GetAddedNodeInfo(details, node);

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Error);
        }

        [Test]
        public async Task GetConnectionCount()
        {
            var obj = await client.Network.GetConnectionCount();

            Assert.IsNotNull(obj);
            Assert.GreaterOrEqual(obj.Result, 0);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetNetTotals()
        {
            var obj = await client.Network.GetNetTotals();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetNetworkInfo()
        {
            var obj = await client.Network.GetNetworkInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task GetPeerInfo()
        {
            var obj = await client.Network.GetPeerInfo();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task ListBanned()
        {
            var obj = await client.Network.ListBanned();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        public async Task Ping()
        {
            var obj = await client.Network.ListBanned();

            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase("192.0.2.113", "add", 2592000)]
        [TestCase("192.0.2.113", "remove", 2592000)]
        public async Task SetBan(string ip, string command, int banTime = 86400, bool absolute = false)
        {
            var obj = await client.Network.SetBan(ip, command, banTime, absolute);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj.Result);
            Assert.IsNull(obj.Error);
        }

        [Test]
        [TestCase(false)]
        [TestCase(true)]
        public async Task SetNetworkActive(bool activate)
        {
            var obj = await client.Network.SetNetworkActive(activate);

            Assert.IsNotNull(obj);
            Assert.AreEqual(obj.Result, activate);
            Assert.IsNull(obj.Error);
        }
    }
}
