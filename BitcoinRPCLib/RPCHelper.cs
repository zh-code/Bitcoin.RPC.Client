﻿using BitcoinRPCLib.Model;
using System.Collections.Generic;

namespace BitcoinRPCLib
{
    internal class RPCHelper
    {
        public static RPCRequestModel GetRequest()
        {
            return new RPCRequestModel { JsonRPC = "1.0" };
        }

        public static RPCRequestModel GetRequest(string method)
        {
            return new RPCRequestModel { JsonRPC = "1.0", Method = method };
        }

        public static RPCRequestModel GetRequest(string method, List<object> @params)
        {
            return new RPCRequestModel { JsonRPC = "1.0", Method = method, Params = @params };
        }
    }
}