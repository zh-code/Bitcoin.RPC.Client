﻿using BitcoinRPCLib.RPC;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace BitcoinRPCLib
{
    public class BitcoinRPCClient
    {
        private HttpClient client;
        private AuthenticationHeaderValue authHeader;

        private IBlockChainRPC blockChainRPC;
        private IControlRPC controlRPC;
        private IGeneratingRPC generatingRPC;
        private IMiningRPC miningRPC;
        private INetworkRPC networkRPC;
        private IRawTransactionRPC rawTransactionRPC;
        private IUtilityRPC utilityRPC;
        private IWalletRPC walletRPC;

        public IBlockChainRPC BlockChain
        {
            get => blockChainRPC ?? (blockChainRPC = new BlockChainRPC(client));
        }

        public IControlRPC Control
        {
            get => controlRPC ?? (controlRPC = new ControlRPC(client));
        }

        public IGeneratingRPC Generating
        {
            get => generatingRPC ?? (generatingRPC = new GeneratingRPC(client));
        }

        public IMiningRPC Mining
        {
            get => miningRPC ?? (miningRPC = new MiningRPC(client));
        }

        public INetworkRPC Network
        {
            get => networkRPC ?? (networkRPC = new NetworkRPC(client));
        }

        public IRawTransactionRPC RawTransaction
        {
            get => rawTransactionRPC ?? (rawTransactionRPC = new RawTransactionRPC(client));
        }

        public IUtilityRPC Utility
        {
            get => utilityRPC ?? (utilityRPC = new UtilityRPC(client));
        }

        public IWalletRPC Wallet
        {
            get => walletRPC ?? (walletRPC = new WalletRPC(client));
        }

        public BitcoinRPCClient(string url, string username, string password)
        {
            client = new HttpClient()
            {
                BaseAddress = new Uri(url)
            };
            authHeader = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));
            Init();
        }

        public BitcoinRPCClient(Uri uri, string username, string password)
        {
            client = new HttpClient()
            {
                BaseAddress = uri
            };
            authHeader = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}")));
            Init();
        }

        public BitcoinRPCClient(string url, NetworkCredential credential)
        {
            client = new HttpClient()
            {
                BaseAddress = new Uri(url)
            };
            authHeader = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{credential.UserName}:{credential.Password}")));
            Init();
        }

        public BitcoinRPCClient(Uri uri, NetworkCredential credential)
        {
            client = new HttpClient()
            {
                BaseAddress = uri
            };
            authHeader = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{credential.UserName}:{credential.Password}")));
            Init();
        }

        private void Init()
        {
            client.DefaultRequestHeaders.Authorization = authHeader;
            client.DefaultRequestHeaders.Add("User-Agent", "zh-code.com Bitcoin RPC .Net Wrapper");
        }

        public void IgnoreSSL()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }
    }
}