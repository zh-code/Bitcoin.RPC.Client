﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Utility
{
    public class GetMemoryInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public MemoryInfoModel Result { get; set; }

        public class MemoryInfoModel
        {
            [JsonProperty("locked")]
            public LockModel Locked { get; set; }

            public class LockModel
            {
                [JsonProperty("used")]
                public int Used { get; set; }

                [JsonProperty("free")]
                public int Free { get; set; }

                [JsonProperty("total")]
                public int Total { get; set; }

                [JsonProperty("locked")]
                public int Locked { get; set; }

                [JsonProperty("chunks_used")]
                public int ChunksUsed { get; set; }

                [JsonProperty("chunks_free")]
                public int ChunksFree { get; set; }
            }
        }
    }
}