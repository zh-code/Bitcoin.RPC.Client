﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Utility
{
    public class CreateMultiSigResponse : ResponseBase
    {
        [JsonProperty("result")]
        public MultiSigModel Result { get; set; }

        public class MultiSigModel
        {
            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("redeemScript")]
            public string redeemScript { get; set; }
        }
    }
}