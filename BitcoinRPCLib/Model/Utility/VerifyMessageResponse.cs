﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Utility
{
    public class VerifyMessageResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}