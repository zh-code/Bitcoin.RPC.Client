﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Utility
{
    public class ValidateAddressResponse : ResponseBase
    {
        [JsonProperty("result")]
        public ValidateAddressModel Result { get; set; }

        public class ValidateAddressModel
        {
            [JsonProperty("isvalid")]
            public bool IsValid { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("scriptPubKey")]
            public string ScriptPubKey { get; set; }

            [JsonProperty("ismine")]
            public bool IsMine { get; set; }

            [JsonProperty("iswatchonly")]
            public bool IsWatchOnly { get; set; }

            [JsonProperty("isscript")]
            public bool IsScript { get; set; }

            [JsonProperty("pubkey")]
            public string Pubkey { get; set; }

            [JsonProperty("iscompressed")]
            public bool IsCompressed { get; set; }

            [JsonProperty("account")]
            public string Account { get; set; }
        }
    }
}