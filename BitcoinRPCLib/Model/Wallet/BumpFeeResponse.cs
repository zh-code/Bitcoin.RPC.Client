﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class BumpFeeResponse : ResponseBase
    {
        [JsonProperty("result")]
        public BumpFeeModel Result { get; set; }

        public class BumpFeeModel
        {
            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("origfee")]
            public decimal OrigFee { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("errors")]
            public List<string> Errors { get; set; }
        }
    }
}