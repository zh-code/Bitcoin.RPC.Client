﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class GetWalletInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public WalletInfoModel Result { get; set; }

        public class WalletInfoModel
        {
            /// <summary>
            /// The name of the wallet
            /// </summary>
            [JsonProperty("walletname")]
            public string WalletName { get; set; }

            /// <summary>
            /// The version number of the wallet
            /// </summary>
            [JsonProperty("walletversion")]
            public int WalletVersion { get; set; }

            /// <summary>
            /// The balance of the wallet. The same as returned by the getbalance RPC with default parameters
            /// </summary>
            [JsonProperty("balance")]
            public decimal Balance { get; set; }

            /// <summary>
            /// The unconfirmed balance of the wallet
            /// </summary>
            [JsonProperty("unconfirmed_balance")]
            public decimal UnconfirmedBalance { get; set; }

            /// <summary>
            /// The total immature balance of the wallet
            /// </summary>
            [JsonProperty("immature_balance")]
            public decimal ImmatureBalance { get; set; }

            /// <summary>
            /// The total number of transactions in the wallet (both spends and receives)
            /// </summary>
            [JsonProperty("txcount")]
            public int TxCount { get; set; }

            /// <summary>
            /// The date as Unix epoch time when the oldest key in the wallet key pool was created; useful for only scanning blocks created since this date for transactions
            /// </summary>
            [JsonProperty("keypoololdest")]
            public int KeyPoolOldest { get; set; }

            /// <summary>
            /// The number of keys in the wallet keypool
            /// </summary>
            [JsonProperty("keypoolsize")]
            public int KeyPoolSize { get; set; }

            /// <summary>
            /// The number of keys that are pre-generated for internal use (used for change outputs, only appears if the wallet is using this feature, otherwise external keys are used)
            /// </summary>
            [JsonProperty("keypoolsize_hd_internal")]
            public int KeyPoolSizeHdInternal { get; set; }

            /// <summary>
            /// Only returned if the wallet was encrypted with the encryptwallet RPC. A Unix epoch date when the wallet will be locked, or 0 if the wallet is currently locked
            /// </summary>
            [JsonProperty("unlocked_until")]
            public int UnlockedUntil { get; set; }

            /// <summary>
            /// The transaction fee configuration currently set
            /// </summary>
            [JsonProperty("paytxfee")]
            public decimal PayTxFee { get; set; }

            /// <summary>
            /// The Hash160 of the HD seed (only present when HD is enabled)
            /// </summary>
            [JsonProperty("hdseedid")]
            public string HdSeedId { get; set; }

            /// <summary>
            /// Alias for hdseedid retained for backwards-compatibility
            /// </summary>
            [JsonProperty("hdmasterkeyid")]
            public string HdMasterKeyId { get; set; }

            /// <summary>
            /// False if privatekeys are disabled for this wallet (enforced watch-only wallet)
            /// </summary>
            [JsonProperty("private_keys_enabled")]
            public bool PrivateKeysEnabled { get; set; }
        }
    }
}