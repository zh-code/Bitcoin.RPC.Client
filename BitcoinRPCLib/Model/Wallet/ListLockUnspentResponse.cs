﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListLockUnspentResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<LockUnspentModel> Result { get; set; }

        public class LockUnspentModel
        {
            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("vout")]
            public int Vout { get; set; }
        }
    }
}