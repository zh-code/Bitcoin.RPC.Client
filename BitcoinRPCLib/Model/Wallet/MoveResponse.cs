﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class MoveResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}