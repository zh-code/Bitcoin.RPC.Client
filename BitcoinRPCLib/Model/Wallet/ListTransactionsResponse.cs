﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListTransactionsResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<TransactionModel> Result { get; set; }

        public class TransactionModel
        {
            [JsonProperty("involvesWatchonly")]
            public bool InvolvesWatchOnly { get; set; }

            [JsonProperty("account")]
            public string Account { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("category")]
            public string Category { get; set; }

            [JsonProperty("amount")]
            public decimal Amount { get; set; }

            [JsonProperty("label")]
            public string Label { get; set; }

            [JsonProperty("vout")]
            public int Vout { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("trusted")]
            public bool Trusted { get; set; }

            [JsonProperty("generated")]
            public bool Generated { get; set; }

            [JsonProperty("blockhash")]
            public string Blockhash { get; set; }

            [JsonProperty("blockindex")]
            public int Blockindex { get; set; }

            [JsonProperty("blocktime")]
            public long Blocktime { get; set; }

            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("walletconflicts")]
            public List<string> WalletConflicts { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("timereceived")]
            public long Timereceived { get; set; }

            [JsonProperty("bip125-replaceable")]
            public string BIP125Replaceable { get; set; }

            [JsonProperty("comment")]
            public string Comment { get; set; }

            [JsonProperty("to")]
            public string To { get; set; }

            [JsonProperty("otheraccount")]
            public string OtherAccount { get; set; }

            [JsonProperty("abandoned")]
            public bool Abandoned { get; set; }
        }
    }
}