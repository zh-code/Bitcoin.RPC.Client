﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class LockUnspentOutput
    {
        /// <summary>
        /// The TXID of the transaction containing the output to lock or unlock, encoded as hex in internal byte order
        /// </summary>
        [JsonProperty("txid")]
        public string TxId { get; set; }

        /// <summary>
        /// The output index number (vout) of the output to lock or unlock. The first output in a transaction has an index of 0
        /// </summary>
        [JsonProperty("vout")]
        public int Vout { get; set; }
    }
}