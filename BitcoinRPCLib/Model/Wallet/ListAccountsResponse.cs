﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListAccountsResponse : ResponseBase
    {
        [JsonProperty("result")]
        public Dictionary<string, decimal> Result { get; set; }
    }
}