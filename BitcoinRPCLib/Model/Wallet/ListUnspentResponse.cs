﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListUnspentResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<UnspentModel> Result { get; set; }

        public class UnspentModel
        {
            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("vout")]
            public int Vout { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("account")]
            public string Account { get; set; }

            [JsonProperty("scriptPubKey")]
            public string ScriptPubKey { get; set; }

            [JsonProperty("redeemScript")]
            public string RedeemScript { get; set; }

            [JsonProperty("amount")]
            public decimal Amount { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("spendable")]
            public bool Spendable { get; set; }

            [JsonProperty("solvable")]
            public bool Solvable { get; set; }
        }
    }
}