﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class SetTxFeeResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}