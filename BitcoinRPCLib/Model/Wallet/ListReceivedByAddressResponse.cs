﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListReceivedByAddressResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<ReceivedByAddressModel> Result { get; set; }

        public class ReceivedByAddressModel
        {
            /// <summary>
            /// Set to true if this address is a watch-only address which has received a spendable payment (that is, a payment with at least the specified number of confirmations and which is not an immature coinbase). Otherwise not returned
            /// </summary>
            [JsonProperty("involvesWatchonly")]
            public bool InvolvesWatchOnly { get; set; }

            /// <summary>
            /// Deprecated: will be removed in a later version of Bitcoin Core
            /// The account the address belongs to.May be the default account, an empty string (“”)
            /// </summary>
            [JsonProperty("address")]
            public string Address { get; set; }

            /// <summary>
            /// The total amount the address has received in bitcoins
            /// </summary>
            [JsonProperty("account")]
            public string Account { get; set; }

            /// <summary>
            /// The number of confirmations of the latest transaction to the address. May be 0 for unconfirmed
            /// </summary>
            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            /// <summary>
            /// The account the address belongs to. May be the default account, an empty string (“”)
            /// </summary>
            [JsonProperty("label")]
            public string Label { get; set; }

            /// <summary>
            /// An array of TXIDs belonging to transactions that pay the address
            /// </summary>
            [JsonProperty("txids")]
            public List<string> TxIds { get; set; }
        }
    }
}