﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class GetAddressesByAccountResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<string> Result { get; set; }
    }
}