﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListAddressGroupingsResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<GroupingModel> Result { get; set; }

        public class GroupingModel
        {
            [JsonProperty("grouping")]
            public List<AddressDetailsModel> AddressDetails { get; set; }

            public class AddressDetailsModel
            {
                [JsonProperty("address")]
                public string Address { get; set; }

                [JsonProperty("balance")]
                public decimal Balance { get; set; }

                [JsonProperty("account")]
                public string Account { get; set; }
            }
        }
    }

    internal class ListAddressGroupingsInternalResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<List<List<object>>> Result { get; set; }
    }
}