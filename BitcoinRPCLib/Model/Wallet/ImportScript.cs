﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ImportScript
    {
        /// <summary>
        /// The script (string) to be imported. Must have either this field or address below
        /// </summary>
        [JsonProperty("scriptPubKey")]
        public string ScriptPubKey { get; set; }

        /// <summary>
        /// The P2PKH or P2SH address to be imported. Must have either this field or scriptPubKey above
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// The creation time of the key in Unix epoch time or the string “now” to substitute the current synced block chain time. The timestamp of the oldest key will determine how far back block chain rescans need to begin. Specify now to bypass scanning for keys which are known to never have been used. Specify 0 to scan the entire block chain. Blocks up to 2 hours before the earliest key creation time will be scanned
        /// </summary>
        [JsonProperty("timestamp")]
        [JsonRequired]
        public long Timestamp { get; set; }

        /// <summary>
        /// Array of strings giving pubkeys that must occur in the scriptPubKey or redeemscript
        /// </summary>
        [JsonProperty("pubkeys")]
        public List<string> PubKeys { get; set; }

        /// <summary>
        /// Array of strings giving private keys whose corresponding public keys must occur in the scriptPubKey or redeemscript
        /// </summary>
        [JsonProperty("keys")]
        public List<string> Keys { get; set; }

        /// <summary>
        /// Stating whether matching outputs should be treated as change rather than incoming payments. The default is false
        /// </summary>
        [JsonProperty("internal")]
        [DefaultValue(false)]
        public bool Internal { get; set; }

        /// <summary>
        /// Stating whether matching outputs should be considered watched even when they’re not spendable. This is only allowed if keys are empty. The default is false
        /// </summary>
        [JsonProperty("watchonly")]
        [DefaultValue(false)]
        public bool WatchOnly { get; set; }

        /// <summary>
        /// Label to assign to the address, only allowed with internal set to false. The default is an empty string (“”)
        /// </summary>
        [JsonProperty("label")]
        public string label { get; set; }
    }
}