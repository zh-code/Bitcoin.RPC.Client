﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class LockUnspentResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}