﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ImportAddress : ImportScript
    {
        [JsonProperty("scriptPubKey")]
        public new ScriptPubKeyModel ScriptPubKey { get; set; }

        public class ScriptPubKeyModel
        {
            [JsonProperty("address")]
            public string Address { get; set; }
        }
    }
}