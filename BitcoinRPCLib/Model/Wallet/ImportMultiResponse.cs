﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ImportMultiResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<ImportResponseModel> Result { get; set; }

        public class ImportResponseModel
        {
            [JsonProperty("success")]
            public bool success { get; set; }

            [JsonProperty("error")]
            public ErrorModel Error { get; set; }
        }
    }
}