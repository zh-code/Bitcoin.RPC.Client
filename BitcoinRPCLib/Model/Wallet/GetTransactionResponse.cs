﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class GetTransactionResponse : ResponseBase
    {
        [JsonProperty("result")]
        public TransactionModel Result { get; set; }

        public class TransactionModel
        {
            [JsonProperty("amount")]
            public decimal Amount { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("blockhash")]
            public string Blockhash { get; set; }

            [JsonProperty("blockindex")]
            public int BlockIndex { get; set; }

            [JsonProperty("blocktime")]
            public long BlockTime { get; set; }

            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("walletconflicts")]
            public List<string> WalletConflicts { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("timereceived")]
            public long TimeReceived { get; set; }

            [JsonProperty("bip125-replaceable")]
            public string BIP125Replaceable { get; set; }

            [JsonProperty("details")]
            public List<DetailModel> Details { get; set; }

            [JsonProperty("hex")]
            public string Hex { get; set; }

            public class DetailModel
            {
                [JsonProperty("account")]
                public string Account { get; set; }

                [JsonProperty("address")]
                public string Address { get; set; }

                [JsonProperty("category")]
                public string Category { get; set; }

                [JsonProperty("amount")]
                public decimal Amount { get; set; }

                [JsonProperty("vout")]
                public int Vout { get; set; }

                [JsonProperty("fee")]
                public decimal Fee { get; set; }
            }
        }
    }
}