﻿namespace BitcoinRPCLib.Model.Wallet
{
    public enum AddressType
    {
        Default,
        Legacy,
        P2SH_Segwit,
        BECH32
    }
}