﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace BitcoinRPCLib.Model.Wallet
{
    public class BumpFeeOption
    {
        /// <summary>
        /// The confirmation target in blocks. Based on this value the new fee will be calculated using the same code as the estimatefee RPC. If not set, the default target of ´6´ blocks will be used
        /// </summary>
        [JsonProperty("confTarget")]
        public int ConfTarget { get; set; }

        /// <summary>
        /// The total fee to pay in satoshis (not the feerate). The actual fee can be higher in rare cases if the change output is close to the dust limit
        /// </summary>
        [JsonProperty("totalFee")]
        public long TotalFee { get; set; }

        /// <summary>
        /// Whether the new transaction should still be BIP 125 replaceable. Even if set to false the transaction may still be replacable, for example if it has unconfirmed ancestors which are replaceable. The default is true
        /// </summary>
        [JsonProperty("replaceable")]
        [DefaultValue(true)]
        public bool Replaceable { get; set; }
    }
}