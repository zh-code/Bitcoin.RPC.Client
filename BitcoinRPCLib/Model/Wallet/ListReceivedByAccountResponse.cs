﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Wallet
{
    public class ListReceivedByAccountResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<ReceivedByAccountModel> Result { get; set; }

        public class ReceivedByAccountModel
        {
            /// <summary>
            /// Set to true if the balance of this account includes a watch-only address which has received a spendable payment (that is, a payment with at least the specified number of confirmations and which is not an immature coinbase). Otherwise not returned
            /// </summary>
            [JsonProperty("involvesWatchonly")]
            public bool InvolvesWatchOnly { get; set; }

            /// <summary>
            /// <see cref="The name of the account"/>
            /// </summary>
            [JsonProperty("account")]
            [JsonRequired]
            public string Account { get; set; }

            /// <summary>
            /// The total amount received by this account in bitcoins
            /// </summary>
            [JsonProperty("amount")]
            [JsonRequired]
            public decimal Amount { get; set; }

            /// <summary>
            /// The number of confirmations received by the last transaction received by this account. May be 0
            /// </summary>
            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }
        }
    }
}