﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Mining
{
    public class PrioritiseTransactionResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}