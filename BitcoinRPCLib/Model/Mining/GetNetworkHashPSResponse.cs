﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Mining
{
    public class GetNetworkHashPSResponse : ResponseBase
    {
        [JsonProperty("result")]
        public long Result { get; set; }
    }
}