﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Mining
{
    public class GetMiningInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public MiningInfoModel Result { get; set; }

        public class MiningInfoModel
        {
            [JsonProperty("blocks")]
            public int Blocks { get; set; }

            [JsonProperty("currentblocksize")]
            public int CurrentBlockSize { get; set; }

            [JsonProperty("currentblockweight")]
            public int CurrentBlockWeight { get; set; }

            [JsonProperty("currentblocktx")]
            public int CurrentBlockTx { get; set; }

            [JsonProperty("difficulty")]
            public decimal Difficulty { get; set; }

            [JsonProperty("errors")]
            public string Errors { get; set; }

            [JsonProperty("warnings")]
            public string Warnings { get; set; }

            [JsonProperty("networkhashps")]
            public decimal NetworkHashPS { get; set; }

            [JsonProperty("pooledtx")]
            public int PooledTx { get; set; }

            [JsonProperty("chain")]
            public string Chain { get; set; }
        }
    }
}