﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Mining
{
    public class GetBlockTemplateResponse : ResponseBase
    {
        [JsonProperty("result")]
        public BlockTemplateModel Result { get; set; }

        public class BlockTemplateModel
        {
            [JsonProperty("capabilities")]
            public List<string> Capabilities { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("rules")]
            public List<string> Rules { get; set; }

            [JsonProperty("vbavailable")]
            public object VbAvailable { get; set; }

            [JsonProperty("vbrequired")]
            public int VbRequired { get; set; }

            [JsonProperty("previousblockhash")]
            public string PreviousBlockHash { get; set; }

            [JsonProperty("transactions")]
            public List<TransactionModel> Transactions { get; set; }

            [JsonProperty("coinbaseaux")]
            public CoinbaseModel Coinbaseaux { get; set; }

            [JsonProperty("coinbasevalue")]
            public long CoinbaseValue { get; set; }

            [JsonProperty("longpollid")]
            public string LongpollId { get; set; }

            [JsonProperty("target")]
            public string Target { get; set; }

            [JsonProperty("mintime")]
            public long Mintime { get; set; }

            [JsonProperty("mutable")]
            public List<string> Mutable { get; set; }

            [JsonProperty("noncerange")]
            public string NonceRange { get; set; }

            [JsonProperty("sigoplimit")]
            public int SigopLimit { get; set; }

            [JsonProperty("sizelimit")]
            public int SizeLimit { get; set; }

            [JsonProperty("weightlimit")]
            public int WeightLimit { get; set; }

            [JsonProperty("curtime")]
            public long Curtime { get; set; }

            [JsonProperty("bits")]
            public string Bits { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }
        }

        public class TransactionModel
        {
            [JsonProperty("data")]
            public string Data { get; set; }

            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("depends")]
            public List<string> Depends { get; set; }

            [JsonProperty("fee")]
            public int Fee { get; set; }

            [JsonProperty("sigops")]
            public int Sigops { get; set; }

            [JsonProperty("weight")]
            public int Weight { get; set; }
        }

        public class CoinbaseModel
        {
            [JsonProperty("flags")]
            public string Flags { get; set; }
        }
    }
}