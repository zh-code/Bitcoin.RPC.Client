﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model
{
    public class StringResponse : ResponseBase
    {
        [JsonProperty("result")]
        public string Result { get; set; }
    }
}