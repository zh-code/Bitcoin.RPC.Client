﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Generating
{
    public class GenerateResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<string> Result { get; set; }
    }
}