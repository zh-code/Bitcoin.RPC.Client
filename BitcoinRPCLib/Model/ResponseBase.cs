﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model
{
    public abstract class ResponseBase
    {
        [JsonProperty("error")]
        public ErrorModel Error { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        public class ErrorModel
        {
            [JsonProperty("code")]
            public int Code { get; set; }

            [JsonProperty("message")]
            public string Message { get; set; }
        }
    }
}