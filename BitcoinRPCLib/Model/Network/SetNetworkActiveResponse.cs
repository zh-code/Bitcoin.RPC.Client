﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Network
{
    public class SetNetworkActiveResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}