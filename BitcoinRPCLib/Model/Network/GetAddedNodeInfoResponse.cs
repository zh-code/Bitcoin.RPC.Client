﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Network
{
    public class GetAddedNodeInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<AddedNodeInfoModel> Result { get; set; }

        public class AddedNodeInfoModel
        {
            [JsonProperty("addednode")]
            public string AddedNode { get; set; }

            [JsonProperty("connected")]
            public bool Connected { get; set; }

            [JsonProperty("addresses")]
            public List<AddressModel> Addresses { get; set; }

            public class AddressModel
            {
                [JsonProperty("address")]
                public string Address { get; set; }

                [JsonProperty("connected")]
                public string Connected { get; set; }
            }
        }
    }
}