﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Network
{
    public class GetPeerInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<PeerInfoModel> Result { get; set; }

        public class PeerInfoModel
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("addr")]
            public string Addr { get; set; }

            [JsonProperty("addrlocal")]
            public string AddrLocal { get; set; }

            [JsonProperty("services")]
            public string Services { get; set; }

            [JsonProperty("relaytxes")]
            public bool RelayTxes { get; set; }

            [JsonProperty("lastsend")]
            public long LastSend { get; set; }

            [JsonProperty("lastrecv")]
            public long LastRecv { get; set; }

            [JsonProperty("bytessent")]
            public int BytesSent { get; set; }

            [JsonProperty("bytesrecv")]
            public int BytesRecv { get; set; }

            [JsonProperty("conntime")]
            public long Conntime { get; set; }

            [JsonProperty("timeoffset")]
            public int TimeOffset { get; set; }

            [JsonProperty("pingtime")]
            public decimal Pingtime { get; set; }

            [JsonProperty("minping")]
            public decimal Minping { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("subver")]
            public string Subver { get; set; }

            [JsonProperty("inbound")]
            public bool Inbound { get; set; }

            [JsonProperty("startingheight")]
            public int StartingHeight { get; set; }

            [JsonProperty("banscore")]
            public int BanScore { get; set; }

            [JsonProperty("synced_headers")]
            public int SyncedHeaders { get; set; }

            [JsonProperty("synced_blocks")]
            public int SyncedBlocks { get; set; }

            [JsonProperty("inflight")]
            public List<string> Inflight { get; set; }

            [JsonProperty("whitelisted")]
            public bool WhiteListed { get; set; }

            public BytessentPerMsgModel bytessent_per_msg { get; set; }
            public BytesrecvPerMsgModel bytesrecv_per_msg { get; set; }

            public class BytesrecvPerMsgModel
            {
                [JsonProperty("getdata")]
                public int GetData { get; set; }

                [JsonProperty("ping")]
                public int Ping { get; set; }

                [JsonProperty("pong")]
                public int Pong { get; set; }

                [JsonProperty("verack")]
                public int Verack { get; set; }

                [JsonProperty("version")]
                public int Version { get; set; }
            }

            public class BytessentPerMsgModel
            {
                [JsonProperty("addr")]
                public int Addr { get; set; }

                [JsonProperty("inv")]
                public int Inv { get; set; }

                [JsonProperty("ping")]
                public int Ping { get; set; }

                [JsonProperty("pong")]
                public int Pong { get; set; }

                [JsonProperty("tx")]
                public int Tx { get; set; }

                [JsonProperty("verack")]
                public int Verack { get; set; }

                [JsonProperty("version")]
                public int Version { get; set; }
            }
        }
    }
}