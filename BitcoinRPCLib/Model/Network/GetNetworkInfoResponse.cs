﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Network
{
    public class GetNetworkInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public NetworkInfoModel Result { get; set; }

        public class NetworkInfoModel
        {
            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("subversion")]
            public string SubVersion { get; set; }

            [JsonProperty("protocolversion")]
            public int ProtocolVersion { get; set; }

            [JsonProperty("localservices")]
            public string LocalServices { get; set; }

            [JsonProperty("localrelay")]
            public bool LocalRelay { get; set; }

            [JsonProperty("timeoffset")]
            public int TimeOffset { get; set; }

            [JsonProperty("connections")]
            public int Connections { get; set; }

            [JsonProperty("networks")]
            public List<NetworkModel> Networks { get; set; }

            [JsonProperty("relayfee")]
            public decimal RelayFee { get; set; }

            [JsonProperty("localaddresses")]
            public List<LocalAddressModel> LocalAddresses { get; set; }

            [JsonProperty("warnings")]
            public string Warnings { get; set; }

            public class NetworkModel
            {
                [JsonProperty("name")]
                public string Name { get; set; }

                [JsonProperty("limited")]
                public bool Limited { get; set; }

                [JsonProperty("reachable")]
                public bool Reachable { get; set; }

                [JsonProperty("proxy")]
                public string Proxy { get; set; }

                [JsonProperty("proxy_randomize_credentials")]
                public bool ProxyRandomizeCredentials { get; set; }
            }

            public class LocalAddressModel
            {
                [JsonProperty("address")]
                public string Address { get; set; }

                [JsonProperty("port")]
                public int Port { get; set; }

                [JsonProperty("score")]
                public int Score { get; set; }
            }
        }
    }
}