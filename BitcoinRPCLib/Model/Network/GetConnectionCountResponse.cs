﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Network
{
    public class GetConnectionCountResponse : ResponseBase
    {
        [JsonProperty("result")]
        public int Result { get; set; }
    }
}