﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.Network
{
    public class ListBannedResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<BannedModel> Result { get; set; }

        public class BannedModel
        {
            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("banned_until")]
            public long BannedUntil { get; set; }

            [JsonProperty("ban_created")]
            public long BanCreated { get; set; }

            [JsonProperty("ban_reason")]
            public string BanReason { get; set; }
        }
    }
}