﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.Network
{
    public class GetNetTotalsResponse : ResponseBase
    {
        [JsonProperty("result")]
        public NetTotalModel Result { get; set; }

        public class NetTotalModel
        {
            [JsonProperty("totalbytesrecv")]
            public long TotalBytesRecv { get; set; }

            [JsonProperty("totalbytessent")]
            public long TotalBytesSent { get; set; }

            [JsonProperty("timemillis")]
            public long TimeMillis { get; set; }

            [JsonProperty("uploadtarget")]
            public UploadtargetModel UploadTarget { get; set; }

            public class UploadtargetModel
            {
                [JsonProperty("timeframe")]
                public int Timeframe { get; set; }

                [JsonProperty("target")]
                public int Target { get; set; }

                [JsonProperty("target_reached")]
                public bool TargetReached { get; set; }

                [JsonProperty("serve_historical_blocks")]
                public bool ServeHistoricalBlocks { get; set; }

                [JsonProperty("bytes_left_in_cycle")]
                public int BytesLeftInCycle { get; set; }

                [JsonProperty("time_left_in_cycle")]
                public int TimeLeftInCycle { get; set; }
            }
        }
    }
}