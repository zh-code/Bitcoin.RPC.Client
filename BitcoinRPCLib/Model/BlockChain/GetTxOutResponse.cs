﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetTxOutResponse : ResponseBase
    {
        [JsonProperty("result")]
        public TxOutResponseModel Result { get; set; }

        public class TxOutResponseModel
        {
            [JsonProperty("bestblock")]
            public string BestBlock { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("value")]
            public decimal value { get; set; }

            [JsonProperty("scriptPubKey")]
            public RawTransaction.DecodeRawTransactionResponse.TransactionModel.VoutModel.ScriptPubKeyModel ScriptPubKey { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("coinbase")]
            public bool Coinbase { get; set; }
        }
    }
}