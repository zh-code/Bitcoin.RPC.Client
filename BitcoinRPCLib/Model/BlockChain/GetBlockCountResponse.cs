﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetBlockCountResponse : ResponseBase
    {
        [JsonProperty("result")]
        public int Result { get; set; }
    }
}