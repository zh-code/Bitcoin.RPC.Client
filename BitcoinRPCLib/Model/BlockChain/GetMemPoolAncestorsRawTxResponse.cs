﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetMemPoolAncestorsRawTxResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<string> Result { get; set; }
    }
}