﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetRawMemPoolJsonResponse : ResponseBase
    {
        [JsonProperty("result")]
        public Dictionary<string, RawMemPoolModel> Result { get; set; }

        public class RawMemPoolModel
        {
            [JsonProperty("fees")]
            public RawMemPoolFeeModel Fees { get; set; }

            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("modifiedfee")]
            public decimal Modifiedfee { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }

            [JsonProperty("descendantcount")]
            public int DescendantCount { get; set; }

            [JsonProperty("descendantsize")]
            public int DescendantSize { get; set; }

            [JsonProperty("descendantfees")]
            public int DescendantFees { get; set; }

            [JsonProperty("ancestorcount")]
            public int AncestorCount { get; set; }

            [JsonProperty("ancestorsize")]
            public int AncestorSize { get; set; }

            [JsonProperty("ancestorfees")]
            public int AncestorFees { get; set; }

            [JsonProperty("wtxid")]
            public string WtxId { get; set; }

            [JsonProperty("depends")]
            public List<string> Depends { get; set; }

            [JsonProperty("spentby")]
            public List<string> Spentby { get; set; }

            public class RawMemPoolFeeModel
            {
                [JsonProperty("base")]
                public decimal Base { get; set; }

                [JsonProperty("modified")]
                public decimal Modified { get; set; }

                [JsonProperty("ancestor")]
                public decimal Ancestor { get; set; }

                [JsonProperty("descendant")]
                public decimal Descendant { get; set; }
            }
        }
    }
}