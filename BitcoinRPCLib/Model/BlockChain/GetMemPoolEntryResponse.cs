﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetMemPoolEntryResponse : ResponseBase
    {
        [JsonProperty("result")]
        public GetMemPoolAncestorsJsonResponse.MemPoolJsonModel Result { get; set; }
    }
}