﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class VerifyChainResponse : ResponseBase
    {
        [JsonProperty("result")]
        public bool Result { get; set; }
    }
}