﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetMemPoolInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public MemPoolInfoModel Result { get; set; }

        public class MemPoolInfoModel
        {
            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("bytes")]
            public int Bytes { get; set; }

            [JsonProperty("usage")]
            public int Usage { get; set; }

            [JsonProperty("maxmempool")]
            public int MaxMemPool { get; set; }

            [JsonProperty("mempoolminfee")]
            public decimal MemPoolMinfee { get; set; }
        }
    }
}