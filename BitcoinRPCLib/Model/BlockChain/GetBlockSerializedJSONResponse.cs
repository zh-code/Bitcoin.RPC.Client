﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetBlockSerializedJsonResponse : ResponseBase
    {
        [JsonProperty("result")]
        public GetBlockSerializedJsonModel Result { get; set; }

        public class GetBlockSerializedJsonModel : GetBlockHeaderJsonResponse.GetBlockHeaderJsonModel
        {
            [JsonProperty("strippedsize")]
            public int Strippedsize { get; set; }

            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("weight")]
            public int Weight { get; set; }

            [JsonProperty("tx")]
            public List<string> Tx { get; set; }
        }
    }
}