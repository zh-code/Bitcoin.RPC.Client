﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetBlockSerializedJsonWithTxResponse : ResponseBase
    {
        [JsonProperty("result")]
        public GetBlockSerializedJsonWithTxModel Result { get; set; }

        public class GetBlockSerializedJsonWithTxModel : GetBlockSerializedJsonResponse.GetBlockSerializedJsonModel
        {
            [JsonProperty("tx")]
            public new List<TransactionModel> Tx { get; set; }

            public class TransactionModel
            {
                [JsonProperty("txid")]
                public string TxId { get; set; }

                [JsonProperty("hash")]
                public string Hash { get; set; }

                [JsonProperty("version")]
                public int Version { get; set; }

                [JsonProperty("size")]
                public int Size { get; set; }

                [JsonProperty("vsize")]
                public int Vsize { get; set; }

                [JsonProperty("weight")]
                public int Weight { get; set; }

                [JsonProperty("locktime")]
                public int Locktime { get; set; }

                [JsonProperty("vin")]
                public List<VinModel> Vin { get; set; }

                [JsonProperty("vout")]
                public List<VoutModel> Vout { get; set; }

                [JsonProperty("hex")]
                public string Hex { get; set; }

                public class VinModel
                {
                    [JsonProperty("coinbase")]
                    public string Coinbase { get; set; }

                    [JsonProperty("sequence")]
                    public long Sequence { get; set; }

                    [JsonProperty("txid")]
                    public string TxId { get; set; }

                    [JsonProperty("vout")]
                    public int Vout { get; set; }

                    [JsonProperty("scriptSig")]
                    public ScriptSignatureModel ScriptSig { get; set; }

                    [JsonProperty("txinwitness")]
                    public List<string> TxinWitness { get; set; }

                    public class ScriptSignatureModel
                    {
                        [JsonProperty("asm")]
                        public string Asm { get; set; }

                        [JsonProperty("hex")]
                        public string Hex { get; set; }
                    }
                }

                public class VoutModel
                {
                    [JsonProperty("value")]
                    public decimal Value { get; set; }

                    [JsonProperty("n")]
                    public int N { get; set; }

                    [JsonProperty("scriptPubKey")]
                    public ScriptPubKeyModel ScriptPubKey { get; set; }

                    public class ScriptPubKeyModel
                    {
                        [JsonProperty("asm")]
                        public string Asm { get; set; }

                        [JsonProperty("hex")]
                        public string Hex { get; set; }

                        [JsonProperty("reqSigs")]
                        public int ReqSigs { get; set; }

                        [JsonProperty("type")]
                        public string Type { get; set; }

                        [JsonProperty("addresses")]
                        public List<string> Addresses { get; set; }
                    }
                }
            }
        }
    }
}