﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class PruneBlockChainResponse : ResponseBase
    {
        [JsonProperty("result")]
        public int Result { get; set; }
    }
}