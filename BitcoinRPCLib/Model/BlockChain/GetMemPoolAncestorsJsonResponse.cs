﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetMemPoolAncestorsJsonResponse : ResponseBase
    {
        [JsonProperty("result")]
        public Dictionary<string, MemPoolJsonModel> Result { get; set; }

        public class MemPoolJsonModel
        {
            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("modifiedfee")]
            public decimal Modifiedfee { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("height")]
            public int height { get; set; }

            [JsonProperty("startingpriority")]
            public decimal StartingPriority { get; set; }

            [JsonProperty("currentpriority")]
            public decimal CurrentPriority { get; set; }

            [JsonProperty("descendantcount")]
            public int DescendantCount { get; set; }

            [JsonProperty("descendantsize")]
            public int DescendantSize { get; set; }

            [JsonProperty("descendantfees")]
            public decimal DescendantFees { get; set; }

            [JsonProperty("ancestorcount")]
            public int AncestorCount { get; set; }

            [JsonProperty("ancestorsize")]
            public int AncestorSize { get; set; }

            [JsonProperty("ancestorfees")]
            public decimal AncestorFees { get; set; }

            [JsonProperty("depends")]
            public List<string> Depends { get; set; }
        }
    }
}