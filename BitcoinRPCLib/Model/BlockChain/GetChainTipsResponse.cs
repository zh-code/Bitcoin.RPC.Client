﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetChainTipsResponse : ResponseBase
    {
        [JsonProperty("result")]
        public List<GetChainTipsModel> Result { get; set; }

        public class GetChainTipsModel
        {
            [JsonProperty("height")]
            public int Height { get; set; }

            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("branchlen")]
            public int branchlen { get; set; }

            [JsonProperty("status")]
            public string Status { get; set; }
        }
    }
}