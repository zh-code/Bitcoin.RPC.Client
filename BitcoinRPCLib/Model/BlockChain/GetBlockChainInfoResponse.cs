﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetBlockChainInfoResponse : ResponseBase
    {
        [JsonProperty("result")]
        public GetBlockChaimInfoModel Result { get; set; }

        public class GetBlockChaimInfoModel
        {
            [JsonProperty("chain")]
            public string Chain { get; set; }

            [JsonProperty("blocks")]
            public int Blocks { get; set; }

            [JsonProperty("headers")]
            public int Headers { get; set; }

            [JsonProperty("bestblockhash")]
            public string BestBlockHash { get; set; }

            [JsonProperty("difficulty")]
            public decimal Difficulty { get; set; }

            [JsonProperty("mediantime")]
            public long Mediantime { get; set; }

            [JsonProperty("verificationprogress")]
            public decimal VerificationProgress { get; set; }

            [JsonProperty("initialblockdownload")]
            public bool InitialBlockDownload { get; set; }

            [JsonProperty("chainwork")]
            public string Chainwork { get; set; }

            [JsonProperty("size_on_disk")]
            public long SizeOnDisk { get; set; }

            [JsonProperty("pruned")]
            public bool Pruned { get; set; }

            [JsonProperty("pruneheight")]
            public int PruneHeight { get; set; }

            [JsonProperty("automatic_pruning")]
            public bool AutomaticPruning { get; set; }

            [JsonProperty("prune_target_size")]
            public long PruneTargetSize { get; set; }

            [JsonProperty("softforks")]
            public List<SoftforkModel> Softforks { get; set; }

            [JsonProperty("bip9_softforks")]
            public Bip9SoftforksModel Bip9Softforks { get; set; }

            [JsonProperty("warnings")]
            public string Warnings { get; set; }

            public class RejectModel
            {
                [JsonProperty("status")]
                public bool Status { get; set; }
            }

            public class SoftforkModel
            {
                [JsonProperty("id")]
                public string Id { get; set; }

                [JsonProperty("version")]
                public int Version { get; set; }

                [JsonProperty("reject")]
                public RejectModel Reject { get; set; }
            }

            public class CsvSegwitModel
            {
                [JsonProperty("status")]
                public string Status { get; set; }

                [JsonProperty("startTime")]
                public long StartTime { get; set; }

                [JsonProperty("timeout")]
                public long timeout { get; set; }

                [JsonProperty("since")]
                public int Since { get; set; }
            }

            public class Bip9SoftforksModel
            {
                [JsonProperty("csv")]
                public CsvSegwitModel Csv { get; set; }

                [JsonProperty("segwit")]
                public CsvSegwitModel Segwit { get; set; }
            }
        }
    }
}