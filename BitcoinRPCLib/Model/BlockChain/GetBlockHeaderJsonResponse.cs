﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.BlockChain
{
    public class GetBlockHeaderJsonResponse : ResponseBase
    {
        [JsonProperty("result")]
        public GetBlockHeaderJsonModel Result { get; set; }

        public class GetBlockHeaderJsonModel
        {
            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }

            [JsonProperty("version")]
            public long Version { get; set; }

            [JsonProperty("versionHex")]
            public string VersionHex { get; set; }

            [JsonProperty("merkleroot")]
            public string Merkleroot { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("mediantime")]
            public long Mediantime { get; set; }

            [JsonProperty("nonce")]
            public long Nonce { get; set; }

            [JsonProperty("bits")]
            public string Bits { get; set; }

            [JsonProperty("difficulty")]
            public decimal Difficulty { get; set; }

            [JsonProperty("chainwork")]
            public string Chainwork { get; set; }

            [JsonProperty("nTx")]
            public int NTx { get; set; }

            [JsonProperty("previousblockhash")]
            public string Previousblockhash { get; set; }

            [JsonProperty("nextblockhash")]
            public string Nextblockhash { get; set; }
        }
    }
}