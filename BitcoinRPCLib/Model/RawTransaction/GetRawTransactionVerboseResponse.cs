﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class GetRawTransactionVerboseResponse : ResponseBase
    {
        [JsonProperty("result")]
        public TransactionModel Result { get; set; }

        public class TransactionModel
        {
            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("vsize")]
            public int Vsize { get; set; }

            [JsonProperty("locktime")]
            public int Locktime { get; set; }

            [JsonProperty("vin")]
            public List<VinModel> Vin { get; set; }

            [JsonProperty("vout")]
            public List<VoutModel> Vout { get; set; }

            [JsonProperty("blockhash")]
            public string Blockhash { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("blocktime")]
            public long BlockTime { get; set; }

            public class VinModel
            {
                [JsonProperty("sequence")]
                public long Sequence { get; set; }

                [JsonProperty("txid")]
                public string TxId { get; set; }

                [JsonProperty("vout")]
                public int Vout { get; set; }

                [JsonProperty("scriptSig")]
                public ScriptSignatureModel ScriptSig { get; set; }

                public class ScriptSignatureModel
                {
                    [JsonProperty("asm")]
                    public string Asm { get; set; }

                    [JsonProperty("hex")]
                    public string Hex { get; set; }
                }
            }

            public class VoutModel
            {
                [JsonProperty("value")]
                public decimal Value { get; set; }

                [JsonProperty("n")]
                public int N { get; set; }

                [JsonProperty("scriptPubKey")]
                public ScriptPubKeyModel ScriptPubKey { get; set; }

                public class ScriptPubKeyModel
                {
                    [JsonProperty("asm")]
                    public string Asm { get; set; }

                    [JsonProperty("hex")]
                    public string Hex { get; set; }

                    [JsonProperty("reqSigs")]
                    public int ReqSigs { get; set; }

                    [JsonProperty("type")]
                    public string Type { get; set; }

                    [JsonProperty("addresses")]
                    public List<string> Addresses { get; set; }
                }
            }
        }
    }
}