﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class FundRawTransactionResponse : ResponseBase
    {
        [JsonProperty("result")]
        public FundRawTransactionModel Result { get; set; }

        public class FundRawTransactionModel
        {
            [JsonProperty("hex")]
            public string Hex { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }

            [JsonProperty("changepos")]
            public int ChangePos { get; set; }
        }
    }
}