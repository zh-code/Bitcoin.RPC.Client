﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class DecodeScriptResponse : ResponseBase
    {
        [JsonProperty("result")]
        public DecodeScriptModel Result { get; set; }

        public class DecodeScriptModel
        {
            [JsonProperty("asm")]
            public string Asm { get; set; }

            [JsonProperty("reqSigs")]
            public int ReqSigs { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("addresses")]
            public List<string> Addresses { get; set; }

            [JsonProperty("p2sh")]
            public string P2SH { get; set; }
        }
    }
}