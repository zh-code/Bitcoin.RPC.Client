﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class RawTransactionInput
    {
        /// <summary>
        /// The TXID of the outpoint to be spent encoded as hex in RPC byte order
        /// </summary>
        [JsonProperty("txid")]
        [JsonRequired]
        public string TxId { get; set; }

        /// <summary>
        /// The output index number (vout) of the outpoint to be spent; the first output in a transaction is index 0
        /// </summary>
        [JsonProperty("vout")]
        [JsonRequired]
        public int Vout { get; set; }

        /// <summary>
        /// The sequence number to use for the input
        /// </summary>
        [JsonProperty("sequence")]
        public int Sequence { get; set; }
    }
}