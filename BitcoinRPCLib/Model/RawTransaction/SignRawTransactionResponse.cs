﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class SignRawTransactionResponse : ResponseBase
    {
        [JsonProperty("result")]
        public RawTransactionModel Result { get; set; }

        public class RawTransactionModel
        {
            [JsonProperty("hex")]
            public string Hex { get; set; }

            [JsonProperty("complete")]
            public bool Complete { get; set; }
        }
    }
}