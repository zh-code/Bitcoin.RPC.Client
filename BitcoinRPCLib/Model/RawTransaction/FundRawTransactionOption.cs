﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class FundRawTransactionOption
    {
        /// <summary>
        /// The bitcoin address to receive the change. If not set, the address is chosen from address pool
        /// </summary>
        [JsonProperty("changeAddress", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ChangeAddress { get; set; }

        /// <summary>
        /// The index of the change output. If not set, the change position is randomly chosen
        /// </summary>
        [JsonProperty("changePosition", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int changePosition { get; set; }

        /// <summary>
        /// Inputs from watch-only addresses are also considered. The default is false
        /// </summary>
        [JsonProperty("includeWatching", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool IncludeWatching { get; set; }

        /// <summary>
        /// The selected outputs are locked after running the rpc call. The default is false
        /// </summary>
        [JsonProperty("lockUnspents", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool LockUnspents { get; set; }

        /// <summary>
        /// Reserves the change output key from the keypool. The default is true. Before 0.14.0, the used keypool key was never marked as change-address key and directly returned to the keypool (leading to address reuse).
        /// </summary>
        [JsonProperty("reserveChangeKey", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(true)]
        public bool ReserveChangeKey { get; set; }

        /// <summary>
        /// The specific feerate you are willing to pay(BTC per KB). If not set, the wallet determines the fee
        /// </summary>
        [JsonProperty("feeRate", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public decimal FeeRate { get; set; }

        /// <summary>
        /// A json array of integers. The fee will be equally deducted from the amount of each specified output. The outputs are specified by their zero-based index, before any change output is added.
        /// A output index number (vout) from which the fee should be subtracted. If multiple vouts are provided, the total fee will be divided by the numer of vouts listed and each vout will have that amount subtracted from it
        /// </summary>
        [JsonProperty("subtractFeeFromOutputs", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public List<int> SubtractFeeFromOutputs { get; set; }
    }
}