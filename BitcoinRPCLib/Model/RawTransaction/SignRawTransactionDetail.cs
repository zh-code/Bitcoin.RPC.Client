﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model.RawTransaction
{
    public class SignRawTransactionDetail
    {
        [JsonProperty("txid")]
        [JsonRequired]
        public string TxId { get; set; }

        [JsonProperty("vout")]
        [JsonRequired]
        public int Vout { get; set; }

        [JsonProperty("scriptPubKey")]
        [JsonRequired]
        public string scriptPubKey { get; set; }

        [JsonProperty("redeemScript")]
        public string RedeemScript { get; set; }
    }
}