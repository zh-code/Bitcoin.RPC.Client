﻿using Newtonsoft.Json;

namespace BitcoinRPCLib.Model
{
    public class DecimalResponse : ResponseBase
    {
        [JsonProperty("result")]
        public decimal Result { get; set; }
    }
}