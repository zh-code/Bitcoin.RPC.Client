﻿using BitcoinRPCLib.Model.RawTransaction;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IRawTransactionRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#createrawtransaction"/>
        /// </summary>
        /// <param name="inputs">An array of objects, each one to be used as an input to the transaction</param>
        /// <param name="outputs">The addresses and amounts to pay
        /// A key/value pair with the address to pay as a string (key) and the amount to pay that address (value) in bitcoins</param>
        /// <param name="lockTime">Indicates the earliest time a transaction can be added to the block chain</param>
        /// <returns></returns>
        Task<CreateRawTransactionResponse> CreateRawTransaction(List<RawTransactionInput> inputs, Dictionary<string, decimal> outputs, int lockTime = 0);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#fundrawtransaction"/>
        /// </summary>
        /// <param name="rawTransactionHex">The hex string of the raw transaction</param>
        /// <param name="option"></param>
        /// <returns></returns>
        Task<FundRawTransactionResponse> FundRawTransaction(string rawTransactionHex, FundRawTransactionOption option);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#decoderawtransaction"/>
        /// </summary>
        /// <param name="rawTransactionHex"></param>
        /// <returns></returns>
        Task<DecodeRawTransactionResponse> DecodeRawTransaction(string rawTransactionHex);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#decoderawtransaction"/>
        /// </summary>
        /// <param name="scriptHex"></param>
        /// <returns></returns>
        Task<DecodeScriptResponse> DecodeScript(string scriptHex);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getrawtransaction"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetRawTransactionResponse> GetRawTransaction(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getrawtransaction"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetRawTransactionVerboseResponse> GetRawTransactionVerbose(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#sendrawtransaction"/>
        /// </summary>
        /// <param name="transactionHex">The serialized transaction to broadcast encoded as hex</param>
        /// <param name="allowHighFees"></param>
        /// <returns></returns>
        Task<SendRawTransactionResponse> SendRawTransaction(string transactionHex, bool allowHighFees = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#signrawtransaction"/>
        /// </summary>
        /// <param name="transactionHex">The transaction to sign as a serialized transaction</param>
        /// <param name="detail">The previous outputs being spent by this transaction</param>
        /// <param name="privateKeys">An array holding private keys. If any keys are provided, only they will be used to sign the transaction (even if the wallet has other matching keys). If this array is empty or not used, and wallet support is enabled, keys from the wallet will be used</param>
        /// <param name="sigHash">The type of signature hash to use for all of the signatures performed. (You must use separate calls to the signrawtransaction RPC if you want to use different signature hash types for different signatures. The allowed values are:
        /// ALL,
        /// NONE,
        /// SINGLE,
        /// ALL|ANYONECANPAY,
        /// NONE|ANYONECANPAY, and
        /// SINGLE|ANYONECANPAY</param>
        /// <returns></returns>
        Task<SignRawTransactionResponse> SignRawTransaction(string transactionHex, SignRawTransactionDetail detail = null,
            List<string> privateKeys = null, string sigHash = "");
    }
}