﻿using BitcoinRPCLib.Model.Utility;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IUtilityRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#createmultisig"/>
        /// </summary>
        /// <param name="amount">The minimum (m) number of signatures required to spend this m-of-n multisig script</param>
        /// <param name="keysOrAddresses">A public key against which signatures will be checked. If wallet support is enabled, this may be a P2PKH address belonging to the wallet—the corresponding public key will be substituted. There must be at least as many keys as specified by the Required parameter, and there may be more keys</param>
        /// <returns></returns>
        Task<CreateMultiSigResponse> CreateMultiSig(int amount, List<string> keysOrAddresses);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#estimatefee"/>
        /// </summary>
        /// <param name="blocks">The maximum number of blocks a transaction should have to wait before it is predicted to be included in a block. Has to be between 2 and 25 blocks</param>
        /// <returns></returns>
        Task<EstimateFeeResponse> EstimateFee(int blocks);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#estimatepriority"/>
        /// </summary>
        /// <param name="blocks">DEPRECATED: will be removed in a later version of Bitcoin Core
        /// The maximum number of blocks a transaction should have to wait before it is predicted to be included in a block based purely on its priority</param>
        /// <returns></returns>
        Task<EstimatePriorityResponse> EstimatePriority(int blocks);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmemoryinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetMemoryInfoResponse> GetMemoryInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#validateaddress"/>
        /// </summary>
        /// <param name="address">The P2PKH or P2SH address to validate encoded in base58check format</param>
        /// <returns></returns>
        Task<ValidateAddressResponse> ValidateAddress(string address);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#verifymessage"/>
        /// </summary>
        /// <param name="address">The P2PKH address corresponding to the private key which made the signature. A P2PKH address is a hash of the public key corresponding to the private key which made the signature.</param>
        /// <param name="signature">The signature created by the signer encoded as base-64 (the format output by the signmessage RPC)</param>
        /// <param name="message">The message exactly as it was signed (e.g. no extra whitespace)</param>
        /// <returns></returns>
        Task<VerifyMessageResponse> VerifyMessage(string address, string signature, string message);
    }
}