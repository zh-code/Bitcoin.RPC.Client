﻿using BitcoinRPCLib.Model.Network;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface INetworkRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#addnode"/>
        /// </summary>
        /// <param name="node">IP:Port</param>
        /// <param name="command"></param>
        /// <returns></returns>
        Task<AddNodeResponse> AddNode(string node, string command);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#clearbanned"/>
        /// </summary>
        /// <returns></returns>
        Task<ClearBannedResponse> ClearBanned();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#disconnectnode"/>
        /// </summary>
        /// <param name="node">IP:Port</param>
        /// <returns></returns>
        Task<DisconnectNodeResponse> DisconnectNode(string node);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getaddednodeinfo"/>
        /// </summary>
        /// <param name="details">Set to true to display detailed information about each added node; set to false to only display the IP address or hostname and port added</param>
        /// <param name="node">IP:Port</param>
        /// <returns></returns>
        Task<GetAddedNodeInfoResponse> GetAddedNodeInfo(bool details, string node = "");

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getconnectioncount"/>
        /// </summary>
        /// <returns></returns>
        Task<GetConnectionCountResponse> GetConnectionCount();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmininginfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetNetTotalsResponse> GetNetTotals();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getnetworkinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetNetworkInfoResponse> GetNetworkInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getpeerinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetPeerInfoResponse> GetPeerInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listbanned"/>
        /// </summary>
        /// <returns></returns>
        Task<ListBannedResponse> ListBanned();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#ping-rpc"/>
        /// </summary>
        /// <returns></returns>
        Task<PingResponse> Ping();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#setban"/>
        /// </summary>
        /// <param name="ip">The node to add or remove as a string in the form of <IP address>. The IP address may be a hostname resolvable through DNS, an IPv4 address, an IPv4-as-IPv6 address, or an IPv6 address</param>
        /// <param name="command">What to do with the IP/Subnet address above. Options are:
        /// 1. "add" to add a node to the addnode list
        /// 2. "remove" to remove a node from the list.If currently connected, this will disconnect immediately</param>
        /// <param name="banTime">Time in seconds how long (or until when if absolute is set) the entry is banned. The default is 24h which can also be overwritten by the -bantime startup argument</param>
        /// <param name="absolute">If set, the bantime must be a absolute timestamp in seconds since epoch (Jan 1 1970 GMT)</param>
        /// <returns></returns>
        Task<SetBanResponse> SetBan(string ip, string command, int banTime = 86400, bool absolute = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#setgenerate"/>
        /// </summary>
        /// <param name="activate"></param>
        /// <returns></returns>
        Task<SetNetworkActiveResponse> SetNetworkActive(bool activate);
    }
}