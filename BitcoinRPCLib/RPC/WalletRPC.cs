﻿using BitcoinRPCLib.Model.Wallet;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class WalletRPC : RPCBase, IWalletRPC
    {
        public WalletRPC(HttpClient client) : base(client)
        {
        }

        public async Task<AbandonTransactionResponse> AbandonTransaction(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            return await HandlePostWithParams<AbandonTransactionResponse>("abandontransaction", @params);
        }

        public async Task<AddMultiSigAddressResponse> AddMultiSigAddress(int numSigRequired, List<string> keysOrAddresses, string accountName = "")
        {
            List<object> @params = new List<object>();
            @params.Add(numSigRequired);
            @params.Add(keysOrAddresses);
            if (!string.IsNullOrWhiteSpace(accountName))
            {
                @params.Add(accountName);
            }
            return await HandlePostWithParams<AddMultiSigAddressResponse>("addmultisigaddress", @params);
        }

        public async Task<AddWitnessAddressResponse> AddWitnessAddress(string address)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            return await HandlePostWithParams<AddWitnessAddressResponse>("addwitnessaddress", @params);
        }

        public async Task<BakcupWalletResponse> BackupWallert(string destination)
        {
            List<object> @params = new List<object>();
            @params.Add(destination);
            return await HandlePostWithParams<BakcupWalletResponse>("backupwallet", @params);
        }

        public async Task<BumpFeeResponse> BumpFee(string txid, BumpFeeOption option = null)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            if (option != null)
            {
                @params.Add(option);
            }
            return await HandlePostWithParams<BumpFeeResponse>("bumpfee", @params);
        }

        public async Task<DumpPrivKeyResponse> DumpPrivKey(string address)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            return await HandlePostWithParams<DumpPrivKeyResponse>("dumpprivkey", @params);
        }

        public async Task<DumpWalletResponse> DumpWallet(string fileName)
        {
            List<object> @params = new List<object>();
            @params.Add(fileName);
            return await HandlePostWithParams<DumpWalletResponse>("dumpwallet", @params);
        }

        public async Task<EncryptWalletResponse> EncryptWallet(string passPhrase)
        {
            List<object> @params = new List<object>();
            @params.Add(passPhrase);
            return await HandlePostWithParams<EncryptWalletResponse>("encryptwallet", @params);
        }

        public async Task<GetAccountResponse> GetAccount(string address)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            return await HandlePostWithParams<GetAccountResponse>("getaccount", @params);
        }

        public async Task<GetAccountAddressResponse> GetAccountAddress(string account)
        {
            List<object> @params = new List<object>();
            @params.Add(account);
            return await HandlePostWithParams<GetAccountAddressResponse>("getaccountaddress", @params);
        }

        public async Task<GetAddressesByAccountResponse> GetAddressesByAccount(string account)
        {
            List<object> @params = new List<object>();
            @params.Add(account);
            return await HandlePostWithParams<GetAddressesByAccountResponse>("getaddressesbyaccount", @params);
        }

        public async Task<GetBalanceResponse> GetBalance(string account = "*", int minConfs = 0, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(account);
            @params.Add(minConfs);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<GetBalanceResponse>("getbalance", @params);
        }

        public async Task<GetNewAddressResponse> GetNewAddress(string account = "", AddressType type = AddressType.Default)
        {
            List<object> @params = new List<object>();
            if (!string.IsNullOrEmpty(account))
            {
                @params.Add(account);
            }
            if (type != AddressType.Default)
            {
                switch (type)
                {
                    case AddressType.BECH32:
                        @params.Add("bech32");
                        break;

                    case AddressType.Legacy:
                        @params.Add("legacy");
                        break;

                    case AddressType.P2SH_Segwit:
                        @params.Add("p2sh-segwit’");
                        break;
                }
            }
            return await HandlePostWithParams<GetNewAddressResponse>("getnewaddress", @params);
        }

        public async Task<GetRawChangeAddressResponse> GetRawChangeAddress() =>
            await HandlePostNoParams<GetRawChangeAddressResponse>("getrawchangeaddress");

        public async Task<GetReceivedByAccountResponse> GetReceivedByAccount(string account, int minConfs)
        {
            List<object> @params = new List<object>();
            @params.Add(account);
            @params.Add(minConfs);
            return await HandlePostWithParams<GetReceivedByAccountResponse>("getreceivedbyaccount", @params);
        }

        public async Task<GetReceivedByAddressResponse> GetReceivedByAddress(string address, int minConfs)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            @params.Add(minConfs);
            return await HandlePostWithParams<GetReceivedByAddressResponse>("getreceivedbyaddress", @params);
        }

        public async Task<GetTransactionResponse> GetTransaction(string txid, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<GetTransactionResponse>("gettransaction", @params);
        }

        public async Task<GetUnconfirmedBalanceResponse> GetUnconfirmedBalance() =>
            await HandlePostNoParams<GetUnconfirmedBalanceResponse>("getunconfirmedbalance");

        public async Task<GetWalletInfoResponse> GetWalletInfo() =>
            await HandlePostNoParams<GetWalletInfoResponse>("getwalletinfo");

        public async Task<ImportAddressResponse> ImportAddress(string addressOrScript, string account = "", bool rescan = true)
        {
            List<object> @params = new List<object>();
            @params.Add(addressOrScript);
            @params.Add(account);
            @params.Add(rescan);
            return await HandlePostWithParams<ImportAddressResponse>("importaddress", @params);
        }

        public async Task<ImportMultiResponse> ImportMultiScript(List<ImportScript> imports, bool rescan = false)
        {
            List<object> @params = new List<object>();
            @params.Add(imports);
            @params.Add(new { rescan = rescan });
            return await HandlePostWithParams<ImportMultiResponse>("importmulti", @params);
        }

        public async Task<ImportMultiResponse> ImportMultiAddress(List<ImportAddress> imports, bool rescan = false)
        {
            List<object> @params = new List<object>();
            @params.Add(imports);
            @params.Add(new { rescan = rescan });
            return await HandlePostWithParams<ImportMultiResponse>("importmulti", @params);
        }

        public async Task<ImportPrunedFundsResponse> ImportPrunedFunds(string rawTransaction, string txOutProof)
        {
            List<object> @params = new List<object>();
            @params.Add(rawTransaction);
            @params.Add(txOutProof);
            return await HandlePostWithParams<ImportPrunedFundsResponse>("importprunedfunds", @params);
        }

        public async Task<ImportPrivKeyResponse> ImportPrivKey(string privateKey, string account = "", bool rescan = true)
        {
            List<object> @params = new List<object>();
            @params.Add(privateKey);
            @params.Add(account);
            @params.Add(rescan);
            return await HandlePostWithParams<ImportPrivKeyResponse>("importprivkey", @params);
        }

        public async Task<ImportWalletResponse> ImportWallet(string fileName)
        {
            List<object> @params = new List<object>();
            @params.Add(fileName);
            return await HandlePostWithParams<ImportWalletResponse>("importwallet", @params);
        }

        public async Task<KeyPoolRefillResponse> KeyPoolRefill(int keyPoolSize)
        {
            List<object> @params = new List<object>();
            @params.Add(keyPoolSize);
            return await HandlePostWithParams<KeyPoolRefillResponse>("keypoolrefill", @params);
        }

        public async Task<ListAccountsResponse> ListAccounts(int minConfs = 0, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(minConfs);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<ListAccountsResponse>("listaccounts", @params);
        }

        public async Task<ListAddressGroupingsResponse> ListAddressGroupings()
        {
            var response = await HandlePostNoParams<ListAddressGroupingsInternalResponse>("listaddressgroupings");

            var ret = new ListAddressGroupingsResponse();

            ret.Error = response.Error;
            ret.Id = response.Id;

            ret.Result = new List<ListAddressGroupingsResponse.GroupingModel>();

            foreach (var group in response.Result)
            {
                var retGroup = new ListAddressGroupingsResponse.GroupingModel();
                retGroup.AddressDetails = new List<ListAddressGroupingsResponse.GroupingModel.AddressDetailsModel>();
                foreach (var item in group)
                {
                    var temp = new ListAddressGroupingsResponse.GroupingModel.AddressDetailsModel();
                    temp.Address = item[0].ToString();
                    if (item.Count > 1)
                    {
                        temp.Balance = Convert.ToDecimal(item[1]);
                        if (item.Count > 2)
                        {
                            temp.Account = item[2].ToString();
                        }
                    }
                    retGroup.AddressDetails.Add(temp);
                }
                ret.Result.Add(retGroup);
            }

            return ret;
        }

        public async Task<ListLockUnspentResponse> ListLockUnspent() =>
            await HandlePostNoParams<ListLockUnspentResponse>("listlockunspent");

        public async Task<ListReceivedByAccountResponse> ListReceivedByAccount(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(confirmations);
            @params.Add(includeEmpty);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<ListReceivedByAccountResponse>("listreceivedbyaccount", @params);
        }

        public async Task<ListReceivedByAddressResponse> ListReceivedByAddress(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(confirmations);
            @params.Add(includeEmpty);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<ListReceivedByAddressResponse>("listreceivedbyaddress", @params);
        }

        public async Task<ListSinceBlockResponse> ListSinceBlock(string headerHash = "", int targetConfirmations = 1, bool includeWatchOnly = false)
        {
            List<object> @params = new List<object>();
            @params.Add(headerHash);
            @params.Add(targetConfirmations);
            @params.Add(includeWatchOnly);
            return await HandlePostWithParams<ListSinceBlockResponse>("listsinceblock", @params);
        }

        public async Task<ListTransactionsResponse> ListTransactions(string account = "*", int amountOfTx = 10, bool skip = false)
        {
            List<object> @params = new List<object>();
            @params.Add(account);
            @params.Add(amountOfTx);
            @params.Add(skip);
            return await HandlePostWithParams<ListTransactionsResponse>("listsinceblock", @params);
        }

        public async Task<ListUnspentResponse> ListUnspent(int minConfs = 1, int maxConfs = 9999999, List<string> addresses = null)
        {
            List<object> @params = new List<object>();
            @params.Add(minConfs);
            @params.Add(maxConfs);
            if (addresses != null)
            {
                @params.Add(addresses);
            }
            return await HandlePostWithParams<ListUnspentResponse>("listunspent", @params);
        }

        public async Task<LockUnspentResponse> LockUnspent(bool unlock, List<LockUnspentOutput> outputs = null)
        {
            List<object> @params = new List<object>();
            @params.Add(unlock);
            if (outputs != null)
            {
                @params.Add(outputs);
            }
            return await HandlePostWithParams<LockUnspentResponse>("lockunspent", @params);
        }

        public async Task<MoveResponse> Move(string from, string to, decimal amount, string comment = "")
        {
            List<object> @params = new List<object>();
            @params.Add(from);
            @params.Add(to);
            @params.Add(amount);
            if (!string.IsNullOrWhiteSpace(comment))
            {
                @params.Add(0);
                @params.Add(comment);
            }
            return await HandlePostWithParams<MoveResponse>("move", @params);
        }

        public async Task<RemovePrunedFundsResponse> RemovePrunedFunds(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            return await HandlePostWithParams<RemovePrunedFundsResponse>("removeprunedfunds", @params);
        }

        public async Task<SendFromResponse> SendFrom(string from, string to, decimal amount, int confirmation = 1, string comment = "", string commentTo = "")
        {
            List<object> @params = new List<object>();
            @params.Add(from);
            @params.Add(to);
            @params.Add(amount);
            @params.Add(confirmation);
            @params.Add(comment);
            @params.Add(commentTo);
            return await HandlePostWithParams<SendFromResponse>("sendfrom", @params);
        }

        public async Task<SendManyResponse> SendMany(string from, Dictionary<string, decimal> outputs, int confirmation = 1, string comment = "", List<string> autoFeeSubtraction = null)
        {
            List<object> @params = new List<object>();
            @params.Add(from);
            @params.Add(outputs);
            @params.Add(confirmation);
            @params.Add(comment);
            if (autoFeeSubtraction != null)
            {
                @params.Add(autoFeeSubtraction);
            }
            return await HandlePostWithParams<SendManyResponse>("sendmany", @params);
        }

        public async Task<SendToAddressResponse> SendToAddress(string to, decimal amount, string comment = "", string commentTo = "", bool autoSubtractFee = false)
        {
            List<object> @params = new List<object>();
            @params.Add(to);
            @params.Add(amount);
            @params.Add(comment);
            @params.Add(commentTo);
            @params.Add(autoSubtractFee);
            return await HandlePostWithParams<SendToAddressResponse>("sendtoaddress", @params);
        }

        public async Task<SetTxFeeResponse> SetTxFee(decimal fee)
        {
            List<object> @params = new List<object>();
            @params.Add(fee);
            return await HandlePostWithParams<SetTxFeeResponse>("settxfee", @params);
        }

        public async Task<SignMessageResponse> SignMessage(string address, string message)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            @params.Add(message);
            return await HandlePostWithParams<SignMessageResponse>("signmessage", @params);
        }

        public async Task<SignMessageWithPrivKeyResponse> SignMessageWithPrivKey(string privKey, string message)
        {
            List<object> @params = new List<object>();
            @params.Add(privKey);
            @params.Add(message);
            return await HandlePostWithParams<SignMessageWithPrivKeyResponse>("signmessagewithprivkey", @params);
        }

        public async Task<SetAccountResponse> SetAccount(string address, string account)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            @params.Add(account);
            return await HandlePostWithParams<SetAccountResponse>("setaccount", @params);
        }

        public async Task<WalletLockResponse> WalletLock() =>
            await HandlePostNoParams<WalletLockResponse>("walletlock");

        public async Task<WalletPassphraseResponse> WalletPassphrase(string passphrase, int seconds)
        {
            List<object> @params = new List<object>();
            @params.Add(passphrase);
            @params.Add(seconds);
            return await HandlePostWithParams<WalletPassphraseResponse>("walletpassphrase", @params);
        }

        public async Task<WalletPassphraseChangeResponse> WalletPassphraseChange(string current, string @new)
        {
            List<object> @params = new List<object>();
            @params.Add(current);
            @params.Add(@new);
            return await HandlePostWithParams<WalletPassphraseChangeResponse>("walletpassphrasechange", @params);
        }
    }
}