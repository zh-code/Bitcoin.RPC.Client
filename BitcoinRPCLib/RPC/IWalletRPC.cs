﻿using BitcoinRPCLib.Model.Wallet;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IWalletRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#abandontransaction"/>
        /// </summary>
        /// <param name="txid">The TXID of the transaction that you want to abandon. The TXID must be encoded as hex in RPC byte order</param>
        /// <returns></returns>
        Task<AbandonTransactionResponse> AbandonTransaction(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#addwitnessaddress"/>
        /// </summary>
        /// <param name="address">A witness address that gets added to a script. Needs to be in the wallet and uncompressed</param>
        /// <returns></returns>
        Task<AddWitnessAddressResponse> AddWitnessAddress(string address);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#addmultisigaddress"/>
        /// </summary>
        /// <param name="numSigRequired">The minimum (m) number of signatures required to spend this m-of-n multisig script</param>
        /// <param name="keysOrAddresses">A public key against which signatures will be checked. Alternatively, this may be a P2PKH address belonging to the wallet—the corresponding public key will be substituted. There must be at least as many keys as specified by the Required parameter, and there may be more keys</param>
        /// <param name="accountName">The account name in which the address should be stored. Default is the default account, “” (an empty string)</param>
        /// <returns></returns>
        Task<AddMultiSigAddressResponse> AddMultiSigAddress(int numSigRequired, List<string> keysOrAddresses, string accountName = "");

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#backupwallet"/>
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        Task<BakcupWalletResponse> BackupWallert(string destination);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#bumpfee"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <param name="option">Additional options</param>
        /// <returns></returns>
        Task<BumpFeeResponse> BumpFee(string txid, BumpFeeOption option = null);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#dumpprivkey"/>
        /// </summary>
        /// <param name="address">The P2PKH address corresponding to the private key you want returned. Must be the address corresponding to a private key in this wallet</param>
        /// <returns></returns>
        Task<DumpPrivKeyResponse> DumpPrivKey(string address);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#dumpwallet"/>
        /// </summary>
        /// <param name="fileName">The file in which the wallet dump will be placed. May be prefaced by an absolute file path. An existing file with that name will be overwritten</param>
        /// <returns></returns>
        Task<DumpWalletResponse> DumpWallet(string fileName);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#encryptwallet"/>
        /// </summary>
        /// <param name="passPhrase"></param>
        /// <returns></returns>
        Task<EncryptWalletResponse> EncryptWallet(string passPhrase);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#getaccountaddress"/>
        /// </summary>
        /// <param name="account">The name of an account. Use an empty string (“”) for the default account. If the account doesn’t exist, it will be created</param>
        /// <returns></returns>
        Task<GetAccountAddressResponse> GetAccountAddress(string account);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#getaccount"/>
        /// </summary>
        /// <param name="address">A P2PKH or P2SH Bitcoin address belonging either to a specific account or the default account (“”)</param>
        /// <returns></returns>
        Task<GetAccountResponse> GetAccount(string address);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#getaddressesbyaccount"/>
        /// </summary>
        /// <param name="account">The name of the account containing the addresses to get. To get addresses from the default account, pass an empty string (“”)</param>
        /// <returns></returns>
        Task<GetAddressesByAccountResponse> GetAddressesByAccount(string account);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getbalance"/>
        /// </summary>
        /// <param name="account">Deprecated: will be removed in a later version of Bitcoin Core
        /// The name of an account to get the balance for. An empty string (“”) is the default account.The string* will get the balance for all accounts(this is the default behavior)</param>
        /// <param name="minConfs"></param>
        /// <param name="includeWatchOnly"></param>
        /// <returns></returns>
        Task<GetBalanceResponse> GetBalance(string account = "*", int minConfs = 0, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getnewaddress"/>
        /// </summary>
        /// <param name="account">The name of the account to put the address in. The default is the default account, an empty string (“”)</param>
        /// <param name="type">The address type to use. Options are ‘legacy’, ‘p2sh-segwit’, and ‘bech32’. Default is set by -addresstype</param>
        /// <returns></returns>
        Task<GetNewAddressResponse> GetNewAddress(string account = "", AddressType type = AddressType.Default);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getrawchangeaddress"/>
        /// </summary>
        /// <returns></returns>
        Task<GetRawChangeAddressResponse> GetRawChangeAddress();

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#getreceivedbyaccount"/>
        /// </summary>
        /// <param name="account">The name of the account containing the addresses to get. For the default account, use an empty string (“”)</param>
        /// <param name="minConfs">the minimum number of confirmations</param>
        /// <returns></returns>
        Task<GetReceivedByAccountResponse> GetReceivedByAccount(string account, int minConfs = 0);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getreceivedbyaddress"/>
        /// </summary>
        /// <param name="address">The address whose transactions should be tallied</param>
        /// <param name="minConfs">the minimum number of confirmations</param>
        /// <returns></returns>
        Task<GetReceivedByAddressResponse> GetReceivedByAddress(string address, int minConfs = 0);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#gettransaction"/>
        /// </summary>
        /// <param name="txid">The TXID of the transaction to get details about. The TXID must be encoded as hex in RPC byte order</param>
        /// <param name="includeWatchOnly">whether to include watch-only addresses in details and calculations</param>
        /// <returns></returns>
        Task<GetTransactionResponse> GetTransaction(string txid, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getunconfirmedbalance"/>
        /// </summary>
        /// <returns></returns>
        Task<GetUnconfirmedBalanceResponse> GetUnconfirmedBalance();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getwalletinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetWalletInfoResponse> GetWalletInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importaddress"/>
        /// </summary>
        /// <param name="addressOrScript">Either a P2PKH or P2SH address encoded in base58check, or a pubkey script encoded as hex</param>
        /// <param name="account">An account name into which the address should be placed. Default is the default account, an empty string(“”)</param>
        /// <param name="rescan">Set to true (the default) to rescan the entire local block database for transactions affecting any address or pubkey script in the wallet (including transaction affecting the newly-added address or pubkey script). Set to false to not rescan the block database (rescanning can be performed at any time by restarting Bitcoin Core with the -rescan command-line argument). Rescanning may take several minutes.</param>
        /// <returns></returns>
        Task<ImportAddressResponse> ImportAddress(string addressOrScript, string account = "", bool rescan = true);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importmulti"/>
        /// </summary>
        /// <param name="imports">An array of JSON objects, each one being an script to be imported</param>
        /// <param name="rescan">Set to true (the default) to rescan the entire local block chain for transactions affecting any imported address or script. Set to false to not rescan after the import. Rescanning may take a considerable amount of time and may require re-downloading blocks if using block chain pruning</param>
        /// <returns></returns>
        Task<ImportMultiResponse> ImportMultiScript(List<ImportScript> imports, bool rescan = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importmulti"/>
        /// </summary>
        /// <param name="imports">An array of JSON objects, each one being an address to be imported</param>
        /// <param name="rescan">Set to true (the default) to rescan the entire local block chain for transactions affecting any imported address or script. Set to false to not rescan after the import. Rescanning may take a considerable amount of time and may require re-downloading blocks if using block chain pruning</param>
        /// <returns></returns>
        Task<ImportMultiResponse> ImportMultiAddress(List<ImportAddress> imports, bool rescan = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importprunedfunds"/>
        /// </summary>
        /// <param name="rawTransaction">A raw transaction in hex funding an already-existing address in wallet</param>
        /// <param name="txOutProof">The hex output from gettxoutproof that contains the transaction</param>
        /// <returns></returns>
        Task<ImportPrunedFundsResponse> ImportPrunedFunds(string rawTransaction, string txOutProof);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importprivkey"/>
        /// </summary>
        /// <param name="privateKey">The private key to import into the wallet encoded in base58check using wallet import format (WIF)</param>
        /// <param name="account">The name of an account to which transactions involving the key should be assigned. The default is the default account, an empty string (“”)</param>
        /// <param name="rescan">Set to true (the default) to rescan the entire local block database for transactions affecting any address or pubkey script in the wallet (including transaction affecting the newly-added address for this private key). Set to false to not rescan the block database (rescanning can be performed at any time by restarting Bitcoin Core with the -rescan command-line argument). Rescanning may take several minutes. Notes: if the address for this key is already in the wallet, the block database will not be rescanned even if this parameter is set</param>
        /// <returns></returns>
        Task<ImportPrivKeyResponse> ImportPrivKey(string privateKey, string account = "", bool rescan = true);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#importwallet"/>
        /// </summary>
        /// <param name="fileName">The file to import. The path is relative to Bitcoin Core’s working directory</param>
        /// <returns></returns>
        Task<ImportWalletResponse> ImportWallet(string fileName);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#keypoolrefill"/>
        /// </summary>
        /// <param name="keyPoolSize">The new size of the keypool; if the number of keys in the keypool is less than this number, new keys will be generated. Default is 100. The value 0 also equals the default. The value specified is for this call only—the default keypool size is not changed</param>
        /// <returns></returns>
        Task<KeyPoolRefillResponse> KeyPoolRefill(int keyPoolSize);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#listaccounts"/>
        /// </summary>
        /// <param name="minConfs">The minimum number of confirmations an externally-generated transaction must have before it is counted towards the balance. Transactions generated by this node are counted immediately. Typically, externally-generated transactions are payments to this wallet and transactions generated by this node are payments to other wallets. Use 0 to count unconfirmed transactions. Default is 1</param>
        /// <param name="includeWatchOnly">If set to true, include watch-only addresses in details and calculations as if they were regular addresses belonging to the wallet. If set to false (the default), treat watch-only addresses as if they didn’t belong to this wallet</param>
        /// <returns></returns>
        Task<ListAccountsResponse> ListAccounts(int minConfs = 0, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listaddressgroupings"/>
        /// </summary>
        /// <returns></returns>
        Task<ListAddressGroupingsResponse> ListAddressGroupings();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listlockunspent"/>
        /// </summary>
        /// <returns></returns>
        Task<ListLockUnspentResponse> ListLockUnspent();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listreceivedbyaccount"/>
        /// </summary>
        /// <param name="confirmations">The minimum number of confirmations an externally-generated transaction must have before it is counted towards the balance. Transactions generated by this node are counted immediately. Typically, externally-generated transactions are payments to this wallet and transactions generated by this node are payments to other wallets. Use 0 to count unconfirmed transactions. Default is 1</param>
        /// <param name="includeEmpty">Set to true to display accounts which have never received a payment. Set to false (the default) to only include accounts which have received a payment. Any account which has received a payment will be displayed even if its current balance is 0</param>
        /// <param name="includeWatchOnly">If set to true, include watch-only addresses in details and calculations as if they were regular addresses belonging to the wallet. If set to false (the default), treat watch-only addresses as if they didn’t belong to this wallet</param>
        /// <returns></returns>
        Task<ListReceivedByAccountResponse> ListReceivedByAccount(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listreceivedbyaddress"/>
        /// </summary>
        /// <param name="confirmations">The minimum number of confirmations an externally-generated transaction must have before it is counted towards the balance. Transactions generated by this node are counted immediately. Typically, externally-generated transactions are payments to this wallet and transactions generated by this node are payments to other wallets. Use 0 to count unconfirmed transactions. Default is 1</param>
        /// <param name="includeEmpty">Set to true to display accounts which have never received a payment. Set to false (the default) to only include accounts which have received a payment. Any account which has received a payment will be displayed even if its current balance is 0</param>
        /// <param name="includeWatchOnly">If set to true, include watch-only addresses in details and calculations as if they were regular addresses belonging to the wallet. If set to false (the default), treat watch-only addresses as if they didn’t belong to this wallet</param>
        /// <returns></returns>
        Task<ListReceivedByAddressResponse> ListReceivedByAddress(int confirmations = 1, bool includeEmpty = false, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listsinceblock"/>
        /// </summary>
        /// <param name="headerHash">The hash of a block header encoded as hex in RPC byte order. All transactions affecting the wallet which are not in that block or any earlier block will be returned, including unconfirmed transactions. Default is the hash of the genesis block, so all transactions affecting the wallet are returned by default</param>
        /// <param name="targetConfirmations">Sets the lastblock field of the results to the header hash of a block with this many confirmations. This does not affect which transactions are returned. Default is 1, so the hash of the most recent block on the local best block chain is returned</param>
        /// <param name="includeWatchOnly">If set to true, include watch-only addresses in details and calculations as if they were regular addresses belonging to the wallet. If set to false (the default), treat watch-only addresses as if they didn’t belong to this wallet</param>
        /// <returns></returns>
        Task<ListSinceBlockResponse> ListSinceBlock(string headerHash = "", int targetConfirmations = 1, bool includeWatchOnly = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listtransactions"/>
        /// </summary>
        /// <param name="account">Deprecated: will be removed in a later version of Bitcoin Core
        /// The name of an account to get transactinos from.Use an empty string (“”) to get transactions for the default account.Default is * to get transactions for all accounts.</param>
        /// <param name="amountOfTx">The number of the most recent transactions to list. Default is 10</param>
        /// <param name="skip">The number of the most recent transactions which should not be returned. Allows for pagination of results. Default is 0</param>
        /// <returns></returns>
        Task<ListTransactionsResponse> ListTransactions(string account = "*", int amountOfTx = 10, bool skip = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#listunspent"/>
        /// </summary>
        /// <param name="minConfs">The minimum number of confirmations the transaction containing an output must have in order to be returned. Use 0 to return outputs from unconfirmed transactions. Default is 1</param>
        /// <param name="maxConfs">The maximum number of confirmations the transaction containing an output may have in order to be returned. Default is 9999999 (~10 million)</param>
        /// <param name="addresses">P2PKH or P2SH address</param>
        /// <returns></returns>
        Task<ListUnspentResponse> ListUnspent(int minConfs = 1, int maxConfs = 9999999, List<string> addresses = null);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#lockunspent"/>
        /// </summary>
        /// <param name="unlock">Set to false to lock the outputs specified in the following parameter. Set to true to unlock the outputs specified. If this is the only argument specified and it is set to true, all outputs will be unlocked; if it is the only argument and is set to false, there will be no change</param>
        /// <param name="outputs">An array of outputs to lock or unlock</param>
        /// <returns></returns>
        Task<LockUnspentResponse> LockUnspent(bool unlock, List<LockUnspentOutput> outputs = null);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#move"/>
        /// </summary>
        /// <param name="from">The name of the account to move the funds from</param>
        /// <param name="to">The name of the account to move the funds to</param>
        /// <param name="amount">The amount of bitcoins to move</param>
        /// <param name="comment">A comment to assign to this move payment</param>
        /// <returns></returns>
        Task<MoveResponse> Move(string from, string to, decimal amount, string comment = "");

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#removeprunedfunds"/>
        /// </summary>
        /// <param name="txid">The hex-encoded id of the transaction you are removing</param>
        /// <returns></returns>
        Task<RemovePrunedFundsResponse> RemovePrunedFunds(string txid);

        /// <summary>
        /// DEPRECATED
        /// <see cref="https://bitcoin.org/en/developer-reference#sendfrom"/>
        /// </summary>
        /// <param name="from">The name of the account from which the bitcoins should be spent. Use an empty string (“”) for the default account</param>
        /// <param name="to">A P2PKH or P2SH address to which the bitcoins should be sent</param>
        /// <param name="amount">The amount to spend in bitcoins. Bitcoin Core will ensure the account has sufficient bitcoins to pay this amount (but the transaction fee paid is not included in the calculation, so an account can spend a total of its balance plus the transaction fee)</param>
        /// <param name="confirmation">The minimum number of confirmations an incoming transaction must have for its outputs to be credited to this account’s balance. Outgoing transactions are always counted, as are move transactions made with the move RPC. If an account doesn’t have a balance high enough to pay for this transaction, the payment will be rejected. Use 0 to spend unconfirmed incoming payments. Default is 1</param>
        /// <param name="comment">A locally-stored (not broadcast) comment assigned to this transaction. Default is no comment</param>
        /// <param name="commentTo">A locally-stored (not broadcast) comment assigned to this transaction. Meant to be used for describing who the payment was sent to. Default is no comment</param>
        /// <returns></returns>
        Task<SendFromResponse> SendFrom(string from, string to, decimal amount, int confirmation = 1, string comment = "", string commentTo = "");

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#sendmany"/>
        /// </summary>
        /// <param name="from">Deprecated: will be removed in a later version of Bitcoin Core
        /// The name of the account from which the bitcoins should be spent.Use an empty string (“”) for the default account.Bitcoin Core will ensure the account has sufficient bitcoins to pay the total amount in the outputs field described below(but the transaction fee paid is not included in the calculation, so an account can spend a total of its balance plus the transaction fee)</param>
        /// <param name="outputs">An object containing key/value pairs corresponding to the addresses and amounts to pay
        /// A key/value pair with a base58check-encoded string containing the P2PKH or P2SH address to pay as the key, and an amount of bitcoins to pay as the value</param>
        /// <param name="confirmation">The minimum number of confirmations an incoming transaction must have for its outputs to be credited to this account’s balance. Outgoing transactions are always counted, as are move transactions made with the move RPC. If an account doesn’t have a balance high enough to pay for this transaction, the payment will be rejected. Use 0 to spend unconfirmed incoming payments. Default is 1</param>
        /// <param name="comment">A locally-stored (not broadcast) comment assigned to this transaction. Default is no comment</param>
        /// <param name="autoFeeSubtraction">An array of addresses. The fee will be equally divided by as many addresses as are entries in this array and subtracted from each address. If this array is empty or not provided, the fee will be paid by the sender</param>
        /// <returns></returns>
        Task<SendManyResponse> SendMany(string from, Dictionary<string, decimal> outputs, int confirmation = 1, string comment = "", List<string> autoFeeSubtraction = null);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#sendtoaddress"/>
        /// </summary>
        /// <param name="to">A P2PKH or P2SH address to which the bitcoins should be sent</param>
        /// <param name="amount">The amount to spent in bitcoins</param>
        /// <param name="comment">A locally-stored (not broadcast) comment assigned to this transaction. Default is no comment</param>
        /// <param name="commentTo">A locally-stored (not broadcast) comment assigned to this transaction. Meant to be used for describing who the payment was sent to. Default is no comment</param>
        /// <param name="autoSubtractFee">The fee will be deducted from the amount being sent. The recipient will receive less bitcoins than you enter in the amount field. Default is false</param>
        /// <returns></returns>
        Task<SendToAddressResponse> SendToAddress(string to, decimal amount, string comment = "", string commentTo = "", bool autoSubtractFee = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#setaccount"/>
        /// </summary>
        /// <param name="address">The P2PKH or P2SH address to put in the account. Must already belong to the wallet</param>
        /// <param name="account">The name of the account in which the address should be placed. May be the default account, an empty string (“”)</param>
        /// <returns></returns>
        Task<SetAccountResponse> SetAccount(string address, string account);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#settxfee"/>
        /// </summary>
        /// <param name="fee">The transaction fee to pay, in bitcoins, for each kilobyte of transaction data. Be careful setting the fee too low—your transactions may not be relayed or included in blocks</param>
        /// <returns></returns>
        Task<SetTxFeeResponse> SetTxFee(decimal fee);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#signmessage"/>
        /// </summary>
        /// <param name="address">A P2PKH address whose private key belongs to this wallet</param>
        /// <param name="message">The message to sign</param>
        /// <returns></returns>
        Task<SignMessageResponse> SignMessage(string address, string message);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#signmessagewithprivkey"/>
        /// </summary>
        /// <param name="privKey"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<SignMessageWithPrivKeyResponse> SignMessageWithPrivKey(string privKey, string message);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#walletlock"/>
        /// </summary>
        /// <returns></returns>
        Task<WalletLockResponse> WalletLock();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#walletpassphrase"/>
        /// </summary>
        /// <param name="passphrase">The passphrase that unlocks the wallet</param>
        /// <param name="seconds">The number of seconds after which the decryption key will be automatically deleted from memory</param>
        /// <returns></returns>
        Task<WalletPassphraseResponse> WalletPassphrase(string passphrase, int seconds);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#walletpassphrasechange"/>
        /// </summary>
        /// <param name="current">The current wallet passphrase</param>
        /// <param name="new">The new passphrase for the wallet</param>
        /// <returns></returns>
        Task<WalletPassphraseChangeResponse> WalletPassphraseChange(string current, string @new);
    }
}