﻿using BitcoinRPCLib.Model.Network;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class NetworkRPC : RPCBase, INetworkRPC
    {
        public NetworkRPC(HttpClient client) : base(client)
        {
        }

        public async Task<AddNodeResponse> AddNode(string node, string command)
        {
            List<object> @params = new List<object>();
            @params.Add(node);
            @params.Add(command);
            return await HandlePostWithParams<AddNodeResponse>("addnode", @params);
        }

        public async Task<ClearBannedResponse> ClearBanned() =>
            await HandlePostNoParams<ClearBannedResponse>("clearbanned");

        public async Task<DisconnectNodeResponse> DisconnectNode(string node)
        {
            List<object> @params = new List<object>();
            @params.Add(node);
            return await HandlePostWithParams<DisconnectNodeResponse>("disconnectnode", @params);
        }

        public async Task<GetAddedNodeInfoResponse> GetAddedNodeInfo(bool details, string node = "")
        {
            List<object> @params = new List<object>();
            @params.Add(details);
            if (!string.IsNullOrWhiteSpace(node))
            {
                @params.Add(node);
            }
            return await HandlePostWithParams<GetAddedNodeInfoResponse>("getaddednodeinfo", @params);
        }

        public async Task<GetConnectionCountResponse> GetConnectionCount() =>
            await HandlePostNoParams<GetConnectionCountResponse>("getconnectioncount");

        public async Task<GetNetTotalsResponse> GetNetTotals() =>
            await HandlePostNoParams<GetNetTotalsResponse>("getnettotals");

        public async Task<GetNetworkInfoResponse> GetNetworkInfo() =>
            await HandlePostNoParams<GetNetworkInfoResponse>("getnetworkinfo");

        public async Task<GetPeerInfoResponse> GetPeerInfo() =>
            await HandlePostNoParams<GetPeerInfoResponse>("getpeerinfo");

        public async Task<ListBannedResponse> ListBanned() =>
            await HandlePostNoParams<ListBannedResponse>("listbanned");

        public async Task<PingResponse> Ping() =>
            await HandlePostNoParams<PingResponse>("ping");

        public async Task<SetBanResponse> SetBan(string ip, string command, int banTime = 86400, bool absolute = false)
        {
            List<object> @params = new List<object>();
            @params.Add(ip);
            @params.Add(command);
            @params.Add(banTime);
            @params.Add(absolute);
            return await HandlePostWithParams<SetBanResponse>("setban", @params);
        }

        public async Task<SetNetworkActiveResponse> SetNetworkActive(bool activate)
        {
            List<object> @params = new List<object>();
            @params.Add(activate);
            return await HandlePostWithParams<SetNetworkActiveResponse>("setnetworkactive", @params);
        }
    }
}