﻿using BitcoinRPCLib.Model.RawTransaction;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class RawTransactionRPC : RPCBase, IRawTransactionRPC
    {
        public RawTransactionRPC(HttpClient client) : base(client)
        {
        }

        public async Task<CreateRawTransactionResponse> CreateRawTransaction(List<RawTransactionInput> inputs, Dictionary<string, decimal> outputs, int lockTime = 0)
        {
            List<object> @params = new List<object>();
            @params.Add(inputs);
            @params.Add(outputs);
            if (lockTime > 0)
            {
                @params.Add(lockTime);
            }
            return await HandlePostWithParams<CreateRawTransactionResponse>("createrawtransaction", @params);
        }

        public async Task<DecodeRawTransactionResponse> DecodeRawTransaction(string rawTransactionHex)
        {
            List<object> @params = new List<object>();
            @params.Add(rawTransactionHex);
            return await HandlePostWithParams<DecodeRawTransactionResponse>("decoderawtransaction", @params);
        }

        public async Task<DecodeScriptResponse> DecodeScript(string scriptHex)
        {
            List<object> @params = new List<object>();
            @params.Add(scriptHex);
            return await HandlePostWithParams<DecodeScriptResponse>("decodescript", @params);
        }

        public async Task<FundRawTransactionResponse> FundRawTransaction(string rawTransactionHex, FundRawTransactionOption option)
        {
            List<object> @params = new List<object>();
            @params.Add(rawTransactionHex);
            @params.Add(option);
            return await HandlePostWithParams<FundRawTransactionResponse>("fundrawtransaction", @params);
        }

        public async Task<GetRawTransactionResponse> GetRawTransaction(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            return await HandlePostWithParams<GetRawTransactionResponse>("getrawtransaction", @params);
        }

        public async Task<GetRawTransactionVerboseResponse> GetRawTransactionVerbose(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(true);
            return await HandlePostWithParams<GetRawTransactionVerboseResponse>("getrawtransaction", @params);
        }

        public async Task<SendRawTransactionResponse> SendRawTransaction(string transactionHex, bool allowHighFees = false)
        {
            List<object> @params = new List<object>();
            @params.Add(transactionHex);
            @params.Add(allowHighFees);
            return await HandlePostWithParams<SendRawTransactionResponse>("sendrawtransaction", @params);
        }

        public async Task<SignRawTransactionResponse> SignRawTransaction(string transactionHex, SignRawTransactionDetail detail = null,
            List<string> privateKeys = null, string sigHash = "")
        {
            List<object> @params = new List<object>();
            @params.Add(transactionHex);
            if (detail != null)
            {
                @params.Add(detail);
            }
            if (privateKeys != null)
            {
                @params.Add(privateKeys);
            }
            if (!string.IsNullOrWhiteSpace(sigHash))
            {
                @params.Add(sigHash);
            }
            return await HandlePostWithParams<SignRawTransactionResponse>("signrawtransaction", @params);
        }
    }
}