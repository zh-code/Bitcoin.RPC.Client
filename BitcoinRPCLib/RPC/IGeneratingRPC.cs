﻿using BitcoinRPCLib.Model.Generating;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IGeneratingRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#generate"/>
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="maxTries"></param>
        /// <returns></returns>
        Task<GenerateResponse> Generate(int blocks, int maxTries = 0);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#generatetoaddress"/>
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="address">P2PKH or P2SH</param>
        /// <param name="maxTries"></param>
        /// <returns></returns>
        Task<GenerateToAddressResponse> GenerateToAddress(int blocks, string address, int maxTries = 0);
    }
}