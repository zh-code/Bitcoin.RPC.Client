﻿using BitcoinRPCLib.Model.Control;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IControlRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#help"/>
        /// </summary>
        /// <param name="RPC"></param>
        /// <returns></returns>
        Task<HelpResponse> Help(string RPC);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#stop"/>
        /// </summary>
        /// <returns></returns>
        Task<StopResponse> Stop();
    }
}