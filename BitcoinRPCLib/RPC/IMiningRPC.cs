﻿using BitcoinRPCLib.Model.Mining;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IMiningRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblocktemplate"/>
        /// </summary>
        /// <returns></returns>
        Task<GetBlockTemplateResponse> GetBlockTemplate();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmininginfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetMiningInfoResponse> GetMiningInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getnetworkhashps"/>
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        Task<GetNetworkHashPSResponse> GetNetworkHashPS(int blocks = 120, int height = -1);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#prioritisetransaction"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <param name="priority">If positive, the priority to add to the transaction in addition to its computed priority; if negative, the priority to subtract from the transaction’s computed priory. Computed priority is the age of each input in days since it was added to the block chain as an output (coinage) times the value of the input in satoshis (value) divided by the size of the serialized transaction (size), which is coinage * value / size</param>
        /// <param name="fee">Satoshis</param>
        /// <returns></returns>
        Task<PrioritiseTransactionResponse> PrioritiseTransaction(string txid, int priority, long fee);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#submitblock"/>
        /// </summary>
        /// <param name="block"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<SubmitBlockResponse> SubmitBlock(string block, object parameters = null);
    }
}