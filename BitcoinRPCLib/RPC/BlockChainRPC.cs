﻿using BitcoinRPCLib.Model.BlockChain;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class BlockChainRPC : RPCBase, IBlockChainRPC
    {
        public BlockChainRPC(HttpClient client) : base(client)
        {
        }

        public async Task<GetBestBlockHashResponse> GetBestBlockHash() =>
            await HandlePostNoParams<GetBestBlockHashResponse>("getbestblockhash");

        public async Task<GetBlockChainInfoResponse> GetBlockChainInfo() =>
            await HandlePostNoParams<GetBlockChainInfoResponse>("getblockchaininfo");

        public async Task<GetBlockCountResponse> GetBlockCount() =>
            await HandlePostNoParams<GetBlockCountResponse>("getblockcount");

        public async Task<GetBlockHashResponse> GetBlockHash(int height)
        {
            List<object> @params = new List<object>();
            @params.Add(height);
            return await HandlePostWithParams<GetBlockHashResponse>("getblockhash", @params);
        }

        public async Task<GetBlockHeaderHexResponse> GetBlockHeaderHex(string hash)
        {
            List<object> @params = new List<object>();
            @params.Add(hash);
            @params.Add(false);
            return await HandlePostWithParams<GetBlockHeaderHexResponse>("getblockheader", @params);
        }

        public async Task<GetBlockHeaderJsonResponse> GetBlockHeaderJson(string hash)
        {
            List<object> @params = new List<object>();
            @params.Add(hash);
            @params.Add(true);
            return await HandlePostWithParams<GetBlockHeaderJsonResponse>("getblockheader", @params);
        }

        public async Task<GetBlockSerializedHexResponse> GetBlockSerializedHex(string hash)
        {
            List<object> @params = new List<object>();
            @params.Add(hash);
            @params.Add(0);
            return await HandlePostWithParams<GetBlockSerializedHexResponse>("getblock", @params);
        }

        public async Task<GetBlockSerializedJsonResponse> GetBlockSerializedJson(string hash)
        {
            List<object> @params = new List<object>();
            @params.Add(hash);
            @params.Add(1);
            return await HandlePostWithParams<GetBlockSerializedJsonResponse>("getblock", @params);
        }

        public async Task<GetBlockSerializedJsonWithTxResponse> GetBlockSerializedJsonWithTx(string hash)
        {
            List<object> @params = new List<object>();
            @params.Add(hash);
            @params.Add(2);
            return await HandlePostWithParams<GetBlockSerializedJsonWithTxResponse>("getblock", @params);
        }

        public async Task<GetChainTipsResponse> GetChainTips() =>
            await HandlePostNoParams<GetChainTipsResponse>("getchaintips");

        public async Task<GetDifficultyResponse> GetDifficulty() =>
            await HandlePostNoParams<GetDifficultyResponse>("getdifficulty");

        public async Task<GetMemPoolAncestorsJsonResponse> GetMemPoolAncestorsJson(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(true);
            return await HandlePostWithParams<GetMemPoolAncestorsJsonResponse>("getmempoolancestors", @params);
        }

        public async Task<GetMemPoolAncestorsRawTxResponse> GetMemPoolAncestorsRawTx(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(false);
            return await HandlePostWithParams<GetMemPoolAncestorsRawTxResponse>("getmempoolancestors", @params);
        }

        public async Task<GetMemPoolDescendantsJsonResponse> GetMemPoolDescendantsJson(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(true);
            return await HandlePostWithParams<GetMemPoolDescendantsJsonResponse>("getmempooldescendants", @params);
        }

        public async Task<GetMemPoolDescendantsRawTxResponse> GetMemPoolDescendantsRawTx(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(false);
            return await HandlePostWithParams<GetMemPoolDescendantsRawTxResponse>("getmempooldescendants", @params);
        }

        public async Task<GetMemPoolEntryResponse> GetMemPoolEntry(string txid)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            return await HandlePostWithParams<GetMemPoolEntryResponse>("getmempoolentry", @params);
        }

        public async Task<GetMemPoolInfoResponse> GetMemPoolInfo() =>
            await HandlePostNoParams<GetMemPoolInfoResponse>("getmempoolinfo");

        public async Task<GetRawMemPoolJsonResponse> GetRawMemPoolJsonResponse()
        {
            List<object> @params = new List<object>();
            @params.Add(true);
            return await HandlePostWithParams<GetRawMemPoolJsonResponse>("getrawmempool", @params);
        }

        public async Task<GetRawMemPoolTxResponse> GetRawMemPoolTxResponse()
        {
            List<object> @params = new List<object>();
            @params.Add(false);
            return await HandlePostWithParams<GetRawMemPoolTxResponse>("getrawmempool", @params);
        }

        public async Task<GetTxOutResponse> GetTxOut(string txid, int vout = 0, bool unconfirmed = false)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(vout);
            @params.Add(unconfirmed);
            return await HandlePostWithParams<GetTxOutResponse>("gettxout", @params);
        }

        public async Task<GetTxOutputProofResponse> GetTxOutProof(List<string> txids, string headerHash)
        {
            List<object> @params = new List<object>();
            @params.Add(txids);
            if (!string.IsNullOrEmpty(headerHash))
            {
                @params.Add(headerHash);
            }
            return await HandlePostWithParams<GetTxOutputProofResponse>("gettxoutproof", @params);
        }

        public async Task<GetTxOutSetInfoResponse> GetTxOutSetInfo() =>
            await HandlePostNoParams<GetTxOutSetInfoResponse>("gettxoutsetinfo");

        public async Task<PreciousBlockResponse> PreciousBlock(string headerHash)
        {
            List<object> @params = new List<object>();
            @params.Add(headerHash);
            return await HandlePostWithParams<PreciousBlockResponse>("preciousblock", @params);
        }

        public async Task<PruneBlockChainResponse> PruneBlockChain(int height)
        {
            List<object> @params = new List<object>();
            @params.Add(height);
            return await HandlePostWithParams<PruneBlockChainResponse>("pruneblockchain", @params);
        }

        public async Task<VerifyChainResponse> VerifyChain(int checkLevel, int numOfBlocks)
        {
            List<object> @params = new List<object>();
            @params.Add(checkLevel);
            @params.Add(numOfBlocks);
            return await HandlePostWithParams<VerifyChainResponse>("verifychain", @params);
        }

        public async Task<VerifyTxOutProofResponse> VerifyTxOutProof(string proof)
        {
            List<object> @params = new List<object>();
            @params.Add(proof);
            return await HandlePostWithParams<VerifyTxOutProofResponse>("verifytxoutproof", @params);
        }
    }
}