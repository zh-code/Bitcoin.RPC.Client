﻿using BitcoinRPCLib.Model.Mining;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class MiningRPC : RPCBase, IMiningRPC
    {
        public MiningRPC(HttpClient client) : base(client)
        {
        }

        public async Task<GetBlockTemplateResponse> GetBlockTemplate() =>
            await HandlePostNoParams<GetBlockTemplateResponse>("getblocktemplate");

        public async Task<GetMiningInfoResponse> GetMiningInfo() =>
            await HandlePostNoParams<GetMiningInfoResponse>("getmininginfo");

        public async Task<GetNetworkHashPSResponse> GetNetworkHashPS(int blocks = 120, int height = -1)
        {
            List<object> @params = new List<object>();
            @params.Add(blocks);
            @params.Add(height);
            return await HandlePostWithParams<GetNetworkHashPSResponse>("getnetworkhashps", @params);
        }

        public async Task<PrioritiseTransactionResponse> PrioritiseTransaction(string txid, int priority, long fee)
        {
            List<object> @params = new List<object>();
            @params.Add(txid);
            @params.Add(priority);
            @params.Add(fee);
            return await HandlePostWithParams<PrioritiseTransactionResponse>("prioritisetransaction", @params);
        }

        public async Task<SubmitBlockResponse> SubmitBlock(string block, object parameters = null)
        {
            List<object> @params = new List<object>();
            @params.Add(block);
            if (parameters != null)
            {
                @params.Add(parameters);
            }
            return await HandlePostWithParams<SubmitBlockResponse>("submitblock", @params);
        }
    }
}