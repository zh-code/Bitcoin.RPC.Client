﻿using BitcoinRPCLib.Model.Utility;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class UtilityRPC : RPCBase, IUtilityRPC
    {
        public UtilityRPC(HttpClient client) : base(client)
        {
        }

        public async Task<CreateMultiSigResponse> CreateMultiSig(int amount, List<string> keysOrAddresses)
        {
            List<object> @params = new List<object>();
            @params.Add(amount);
            @params.Add(keysOrAddresses);
            return await HandlePostWithParams<CreateMultiSigResponse>("createmultisig", @params);
        }

        public async Task<EstimateFeeResponse> EstimateFee(int blocks)
        {
            List<object> @params = new List<object>();
            @params.Add(blocks);
            return await HandlePostWithParams<EstimateFeeResponse>("estimatefee", @params);
        }

        public async Task<EstimatePriorityResponse> EstimatePriority(int blocks)
        {
            List<object> @params = new List<object>();
            @params.Add(blocks);
            return await HandlePostWithParams<EstimatePriorityResponse>("estimatepriority", @params);
        }

        public async Task<GetMemoryInfoResponse> GetMemoryInfo() =>
            await HandlePostNoParams<GetMemoryInfoResponse>("getmemoryinfo");

        public async Task<ValidateAddressResponse> ValidateAddress(string address)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            return await HandlePostWithParams<ValidateAddressResponse>("validateaddress", @params);
        }

        public async Task<VerifyMessageResponse> VerifyMessage(string address, string signature, string message)
        {
            List<object> @params = new List<object>();
            @params.Add(address);
            @params.Add(signature);
            @params.Add(message);
            return await HandlePostWithParams<VerifyMessageResponse>("verifymessage", @params);
        }
    }
}