﻿using BitcoinRPCLib.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class RPCBase
    {
        private HttpClient client;
        private JsonSerializerSettings JsonSetting;

        public RPCBase(HttpClient client)
        {
            this.client = client;
            JsonSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
        }

        public async Task<T> HandlePostNoParams<T>(string method) where T : class
        {
            var request = RPCHelper.GetRequest(method);
            return await SubmitRequest<T>(request);
        }

        public async Task<T> HandlePostWithParams<T>(string method, List<object> @params)
        {
            var request = RPCHelper.GetRequest(method, @params);
            return await SubmitRequest<T>(request);
        }

        private async Task<T> SubmitRequest<T>(RPCRequestModel request)
        {
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/Json");
            using (var response = await client.PostAsync(client.BaseAddress, content))
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync(), JsonSetting);
                }
                catch
                {
                    return default(T);
                }
            }
        }
    }
}