﻿using BitcoinRPCLib.Model.Control;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class ControlRPC : RPCBase, IControlRPC
    {
        public ControlRPC(HttpClient client) : base(client)
        {
        }

        public async Task<HelpResponse> Help(string RPC)
        {
            List<object> @params = new List<object>();
            @params.Add(RPC);
            return await HandlePostWithParams<HelpResponse>("help", @params);
        }

        public async Task<StopResponse> Stop() =>
            await HandlePostNoParams<StopResponse>("stop");
    }
}