﻿using BitcoinRPCLib.Model.BlockChain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    public interface IBlockChainRPC
    {
        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getbestblockhash"/>
        /// </summary>
        /// <returns></returns>
        Task<GetBestBlockHashResponse> GetBestBlockHash();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblock"/>
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<GetBlockSerializedHexResponse> GetBlockSerializedHex(string hash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblock"/>
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<GetBlockSerializedJsonResponse> GetBlockSerializedJson(string hash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblock"/>
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<GetBlockSerializedJsonWithTxResponse> GetBlockSerializedJsonWithTx(string hash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblockchaininfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetBlockChainInfoResponse> GetBlockChainInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblockcount"/>
        /// </summary>
        /// <returns></returns>
        Task<GetBlockCountResponse> GetBlockCount();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblockhash"/>
        /// </summary>
        /// <param name="height"></param>
        /// <returns></returns>
        Task<GetBlockHashResponse> GetBlockHash(int height);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblockheader"/>
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<GetBlockHeaderHexResponse> GetBlockHeaderHex(string hash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getblockheader"/>
        /// </summary>
        /// <param name="hash"></param>
        /// <returns></returns>
        Task<GetBlockHeaderJsonResponse> GetBlockHeaderJson(string hash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getchaintips"/>
        /// </summary>
        /// <returns></returns>
        Task<GetChainTipsResponse> GetChainTips();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getdifficulty"/>
        /// </summary>
        /// <returns></returns>
        Task<GetDifficultyResponse> GetDifficulty();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolancestors"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetMemPoolAncestorsRawTxResponse> GetMemPoolAncestorsRawTx(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolancestors"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetMemPoolAncestorsJsonResponse> GetMemPoolAncestorsJson(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolancestors"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetMemPoolDescendantsRawTxResponse> GetMemPoolDescendantsRawTx(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolancestors"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetMemPoolDescendantsJsonResponse> GetMemPoolDescendantsJson(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolentry"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <returns></returns>
        Task<GetMemPoolEntryResponse> GetMemPoolEntry(string txid);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getmempoolinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetMemPoolInfoResponse> GetMemPoolInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getrawmempool"/>
        /// </summary>
        /// <returns></returns>
        Task<GetRawMemPoolTxResponse> GetRawMemPoolTxResponse();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#getrawmempool"/>
        /// </summary>
        /// <returns></returns>
        Task<GetRawMemPoolJsonResponse> GetRawMemPoolJsonResponse();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#gettxout"/>
        /// </summary>
        /// <param name="txid"></param>
        /// <param name="vout"></param>
        /// <param name="unconfirmed"></param>
        /// <returns></returns>
        Task<GetTxOutResponse> GetTxOut(string txid, int vout = 0, bool unconfirmed = false);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#gettxoutproof"/>
        /// </summary>
        /// <param name="txids"></param>
        /// <param name="blockHeader"></param>
        /// <returns></returns>
        Task<GetTxOutputProofResponse> GetTxOutProof(List<string> txids, string headerHash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#gettxoutsetinfo"/>
        /// </summary>
        /// <returns></returns>
        Task<GetTxOutSetInfoResponse> GetTxOutSetInfo();

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#preciousblock"/>
        /// </summary>
        /// <param name="headerHash"></param>
        /// <returns></returns>
        Task<PreciousBlockResponse> PreciousBlock(string headerHash);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#pruneblockchain"/>
        /// </summary>
        /// <param name="height"></param>
        /// <returns></returns>
        Task<PruneBlockChainResponse> PruneBlockChain(int height);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#verifychain"/>
        /// </summary>
        /// <remarks>
        /// 0. Read from disk to ensure the files are accessible
        /// 1. Ensure each block is valid
        /// 2. Make sure undo files can be read from disk and are in a valid format
        /// 3. Test each block undo to ensure it results in correct state
        /// 4. After undoing blocks, reconnect them to ensure they reconnect correctly
        /// </remarks>
        /// <param name="checkLevel"></param>
        /// <param name="numOfBlocks"></param>
        /// <returns></returns>
        Task<VerifyChainResponse> VerifyChain(int checkLevel, int numOfBlocks);

        /// <summary>
        /// <see cref="https://bitcoin.org/en/developer-reference#verifytxoutproof"/>
        /// </summary>
        /// <param name="proof">A hex-encoded proof</param>
        /// <returns></returns>
        Task<VerifyTxOutProofResponse> VerifyTxOutProof(string proof);
    }
}