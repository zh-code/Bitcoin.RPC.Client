﻿using BitcoinRPCLib.Model.Generating;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinRPCLib.RPC
{
    internal class GeneratingRPC : RPCBase, IGeneratingRPC
    {
        public GeneratingRPC(HttpClient client) : base(client)
        {
        }

        public async Task<GenerateResponse> Generate(int blocks, int maxTries = 0)
        {
            List<object> @params = new List<object>();
            @params.Add(blocks);
            if (maxTries > 0)
            {
                @params.Add(maxTries);
            }
            return await HandlePostWithParams<GenerateResponse>("generate", @params);
        }

        public async Task<GenerateToAddressResponse> GenerateToAddress(int blocks, string address, int maxTries = 0)
        {
            List<object> @params = new List<object>();
            @params.Add(blocks);
            @params.Add(address);
            if (maxTries > 0)
            {
                @params.Add(maxTries);
            }
            return await HandlePostWithParams<GenerateToAddressResponse>("generatetoaddress", @params);
        }
    }
}